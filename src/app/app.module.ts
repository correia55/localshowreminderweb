import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppRoutingModule } from './app-routing.module';
import { NgxLoadingModule } from 'ngx-loading';
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';

import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatMenuModule } from '@angular/material/menu';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatInputModule } from '@angular/material/input';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatTabsModule } from '@angular/material/tabs';

import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';
import { SearchShowDBComponent } from './components/search-show-db/search-show-db.component';
import { SearchListingsComponent } from './components/search-listings/search-listings.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { UserCornerComponent } from './components/user-corner/user-corner.component';
import { ShowModalComponent } from './components/show-modal/show-modal.component';
import { AlarmSectionComponent } from './components/alarm-section/alarm-section.component';
import { ReminderListComponent } from './components/reminder-list/reminder-list.component';
import { AlarmListComponent } from './components/alarm-list/alarm-list.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { SidenavListComponent } from './components/sidenav-list/sidenav-list.component';
import { AboutComponent } from './components/about/about.component';
import { BottomBarComponent } from './components/bottom-bar/bottom-bar.component';
import { CreateAlarmModalComponent } from './components/create-alarm-modal/create-alarm-modal.component';
import { CreateReminderModalComponent } from './components/create-reminder-modal/create-reminder-modal.component';
import { UpdateAlarmModalComponent } from './components/update-alarm-modal/update-alarm-modal.component';
import { EmailConfirmationComponent } from './components/email-confirmation/email-confirmation.component';
import { SettingsComponent } from './components/settings/settings.component';
import { EmailChangeComponent } from './components/email-change/email-change.component';
import { PasswordChangeComponent } from './components/password-change/password-change.component';
import { PasswordRecoveryComponent } from './components/password-recovery/password-recovery.component';
import { PasswordRecoveryChangeComponent } from './components/password-recovery-change/password-recovery-change.component';
import { InexistantPageComponent } from './components/inexistant-page/inexistant-page.component';
import { RegistrationFormComponent } from './components/registration-form/registration-form.component';
import { DisplaySessionsComponent } from './components/display-sessions/display-sessions.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { UpdateReminderModalComponent } from './components/update-reminder-modal/update-reminder-modal.component';
import { HighlightsComponent } from './components/highlights/highlights.component';
import { DbShowCardComponent } from './components/db-show-card/db-show-card.component';
import { NguiInViewComponent } from './components/ngui-in-view/ngui-in-view.component';

import { AlarmCreatedSnackBarComponent } from './components/snack-bar/alarm-created-snackbar-component';
import { AlarmFailedSnackBarComponent } from './components/snack-bar/alarm-failed-snackbar-component';
import { AlarmUpdatedSnackBarComponent } from './components/snack-bar/alarm-updated-snackbar-component';
import { AlarmDeletedSnackBarComponent } from './components/snack-bar/alarm-deleted-snackbar-component';
import { CheckEmailSnackBarComponent } from './components/snack-bar/check-email-snackbar-component';
import { CredentialsSnackBarComponent } from './components/snack-bar/credentials-snackbar-component';
import { GenericProblemSnackBarComponent } from './components/snack-bar/generic-problem-snackbar-component';
import { SessionExpiredSnackBarComponent } from './components/snack-bar/session-expired-snackbar-component';
import { PasswordChangedSnackBarComponent } from './components/snack-bar/password-changed-snackbar-component';
import { ReminderCreatedSnackBarComponent } from './components/snack-bar/reminder-created-snackbar-component';
import { ReminderDeletedSnackBarComponent } from './components/snack-bar/reminder-deleted-snackbar-component';
import { ReminderFailedSnackBarComponent } from './components/snack-bar/reminder-failed-snackbar-component';
import { ReminderUpdatedSnackBarComponent } from './components/snack-bar/reminder-updated-snackbar-component';
import { SomethingWrongSnackBarComponent } from './components/snack-bar/something-wrong-snackbar-component';
import { UnverifiedAccountSnackBarComponent } from './components/snack-bar/unverified-account-snackbar-component';

import { TokenService } from './services/token/token.service';
import { AlarmService } from './services/alarm/alarm.service';
import { ReminderService } from './services/reminder/reminder.service';
import { SettingsService } from './services/settings/settings.service';
import { ChannelsService } from './services/channels/channels.service';
import { ManagerService } from './services/manager/manager.service';
import { HighlightsService } from './services/highlights/highlights.service';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    SearchShowDBComponent,
    SearchListingsComponent,
    LoginFormComponent,
    UserCornerComponent,
    ShowModalComponent,
    AlarmSectionComponent,
    ReminderListComponent,
    ToolbarComponent,
    SidenavListComponent,
    AboutComponent,
    BottomBarComponent,
    CreateAlarmModalComponent,
    UpdateAlarmModalComponent,
    EmailConfirmationComponent,
    SettingsComponent,
    EmailChangeComponent,
    PasswordChangeComponent,
    PasswordRecoveryComponent,
    PasswordRecoveryChangeComponent,
    InexistantPageComponent,
    CreateReminderModalComponent,
    UpdateReminderModalComponent,
    RegistrationFormComponent,
    CreateReminderModalComponent,
    UpdateReminderModalComponent,
    AlarmListComponent,
    DisplaySessionsComponent,
    PrivacyPolicyComponent,
    HighlightsComponent,
    DbShowCardComponent,
    NguiInViewComponent,

    AlarmCreatedSnackBarComponent,
    AlarmUpdatedSnackBarComponent,
    AlarmDeletedSnackBarComponent,
    AlarmFailedSnackBarComponent,
    GenericProblemSnackBarComponent,
    SessionExpiredSnackBarComponent,
    CheckEmailSnackBarComponent,
    CredentialsSnackBarComponent,
    PasswordChangedSnackBarComponent,
    ReminderCreatedSnackBarComponent,
    ReminderUpdatedSnackBarComponent,
    ReminderDeletedSnackBarComponent,
    ReminderFailedSnackBarComponent,
    SomethingWrongSnackBarComponent,
    UnverifiedAccountSnackBarComponent
  ],
  imports: [
    BrowserModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatTableModule,
    MatIconModule,
    HttpClientModule,
    MatDialogModule,
    MatCardModule,
    MatChipsModule,
    MatMenuModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatListModule,
    MatSidenavModule,
    MatPaginatorModule,
    MatSlideToggleModule,
    MatInputModule,
    MatSelectModule,
    MatButtonToggleModule,
    MatTabsModule,
    NgxLoadingModule.forRoot({}),
    SocialLoginModule
  ],
  providers: [TokenService, AlarmService, ReminderService, SettingsService, ChannelsService, HighlightsService,
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '977035527853-98nkbsbte0qvkeitp77rk3fnk85gdmeo.apps.googleusercontent.com'
            )
          }
        ]
      } as SocialAuthServiceConfig,
    }],
  bootstrap: [AppComponent],
  entryComponents: [
    ShowModalComponent,
    AlarmSectionComponent,
    CreateAlarmModalComponent,
    UpdateAlarmModalComponent,
    CreateReminderModalComponent,
    UpdateReminderModalComponent,
    DisplaySessionsComponent,

    AlarmCreatedSnackBarComponent,
    AlarmUpdatedSnackBarComponent,
    AlarmDeletedSnackBarComponent,
    AlarmFailedSnackBarComponent,
    GenericProblemSnackBarComponent,
    SessionExpiredSnackBarComponent,
    CheckEmailSnackBarComponent,
    CredentialsSnackBarComponent,
    PasswordChangedSnackBarComponent,
    ReminderCreatedSnackBarComponent,
    ReminderUpdatedSnackBarComponent,
    ReminderDeletedSnackBarComponent,
    ReminderFailedSnackBarComponent,
    SomethingWrongSnackBarComponent,
    UnverifiedAccountSnackBarComponent
  ],
})
export class AppModule { }
