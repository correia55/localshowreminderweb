import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ThemeService } from './services/theme/theme.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Local Show Reminder';

  // The snack bars
  @ViewChild('cookiesSnackBar', { static: true })
  private cookiesSnackBar: TemplateRef<any>;

  constructor(private snackBar: MatSnackBar, public themeService: ThemeService) { }

  ngOnInit() {
    const cookiesAccepted = localStorage.getItem('cookies');

    if (cookiesAccepted == null) {
      this.snackBar.openFromTemplate(this.cookiesSnackBar, {});
    }
  }

  /**
   * Clicked the OK button in the cookies snackbar.
   */
  clikedOk() {
    this.snackBar.dismiss();
    localStorage.setItem('cookies', 'true');
  }
}
