import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

import { ApiRequestsService } from '../api-requests/api-requests.service';
import { AlarmCreatedSnackBarComponent } from '../../components/snack-bar/alarm-created-snackbar-component';
import { AlarmFailedSnackBarComponent } from '../../components/snack-bar/alarm-failed-snackbar-component';
import { TokenService } from '../token/token.service';
import { AlarmModalData } from '../../structs/AlarmModalData';
import { AlarmUpdatedSnackBarComponent } from '../../components/snack-bar/alarm-updated-snackbar-component';
import { AlarmDeletedSnackBarComponent } from '../../components/snack-bar/alarm-deleted-snackbar-component';
import { Alarm } from 'src/app/structs/Alarm';
import { BehaviorSubject } from 'rxjs';
import { Utilities } from '../../utilities';
import { ACCESS_TOKEN_ERROR } from 'src/app/constants';

@Injectable({
  providedIn: 'root'
})
export class AlarmService {
  public alarms = new BehaviorSubject<Alarm[]>([]);
  public showLoading = true;

  constructor(private apiRequestsService: ApiRequestsService,
              private tokenService: TokenService) {
    // This obtains the current list of alarms, if the user is logged in
    if (tokenService.validateRefreshToken()) {
      this.getAlarmList();
    }
  }

  /**
   * Get the list of alarms of the user.
   */
  public getAlarmList() {
    return this.getAlarms(0);
  }

  /**
   * Get the list of alarms of the user.
   *
   * @param callRepetition the current call of this function.
   */
  private getAlarms(callRepetition: number) {
    if (callRepetition > 1) {
      console.log('There was an error, we were unable to obtain the list of alarms!');
      return;
    }

    if (callRepetition === 0) {
      this.showLoading = true;
    }

    try {
      const prepareResponse = this.apiRequestsService.prepareGetAlarms();

      if (prepareResponse[0]) {
        prepareResponse[1].subscribe(response => {
          this.alarms.next(Utilities.getAlarmsWithRegexList(response['alarm_list']));

          this.showLoading = false;
        });
      } else {
        prepareResponse[1].subscribe(
          response => {
            this.tokenService.saveAccessToken(response['token']);
            this.getAlarms(callRepetition + 1);
          },
          // Refresh token is no longer valid
          error => {
            if (error.error !== ACCESS_TOKEN_ERROR) {
              this.tokenService.deleteRefreshToken();
            }

            this.showLoading = false;
          }
        );
      }
    } catch (error) {
      if (error.message === this.apiRequestsService.ERROR_LOGIN) {
        /*
          Since this is only performed at the beginning, there's no need to warn the user.
        */
      }

      this.showLoading = false;
    }
  }

  /**
   * Create a alarm associated with the user.
   *
   * @param data the data.
   * @param snackBar the snackbar.
   * @param callRepetition the current call of this function.
   */
  public createAlarm(data: AlarmModalData, snackBar: MatSnackBar, callRepetition: number) {
    if (callRepetition > 1) {
      console.log('There was an error, we were unable to register the alarm!');
      return;
    }

    // If a alarm already exists, use update
    if (data.alarmId != null) {
      return;
    }

    if (callRepetition === 0) {
      this.showLoading = true;
    }

    let prepareResponse;

    try {
      prepareResponse = this.apiRequestsService.prepareAlarmRegistration(data.showTitle,
        data.showIsMovie, data.alarmType.toString(), data.alarmSeason,
        data.alarmEpisode, data.traktId, data.showLanguage);
    } catch (error) {
      if (error.message === this.apiRequestsService.ERROR_LOGIN) {
        console.log('Failed to get list of alarms, login required!');
      }

      this.showLoading = false;
      return;
    }

    // If the access token is still valid
    if (prepareResponse[0]) {
      prepareResponse[1].subscribe(
        response => {
          snackBar.openFromComponent(AlarmCreatedSnackBarComponent, {
            duration: 5000
          });

          this.alarms.next(Utilities.getAlarmsWithRegexList(response['alarm_list']));
          this.showLoading = false;
        },
        error => {
          snackBar.openFromComponent(AlarmFailedSnackBarComponent, {
            duration: 5000
          });

          this.showLoading = false;
        }
      );
    } else { // If a new access token is needed
      prepareResponse[1].subscribe(
        response => {
          this.tokenService.saveAccessToken(response.token);
          this.createAlarm(data, snackBar, callRepetition + 1);
        },
        // Refresh token is no longer valid
        error => {
          if (error.error !== ACCESS_TOKEN_ERROR) {
            this.tokenService.deleteRefreshToken();
          }

          snackBar.openFromComponent(AlarmFailedSnackBarComponent, {
            duration: 5000
          });

          this.showLoading = false;
        }
      );
    }
  }

  /**
   * Update a alarm associated with the user.
   *
   * @param data the data.
   * @param snackBar the snackbar.
   * @param callRepetition the current call of this function.
   */
  public updateAlarm(data: AlarmModalData, snackBar: MatSnackBar, callRepetition: number) {
    if (callRepetition > 1) {
      console.log('There was an error, we were unable to register your alarm!');
      return;
    }

    // If a alarm does not exist, use create
    if (data.alarmId == null) {
      return;
    }

    for (let i = 0, size = this.alarms.getValue().length; i < size; i++) {
      const r = this.alarms.getValue()[i];

      if (r.id === data.alarmId) {
        if (r.show_season === data.alarmSeason && r.show_episode === data.alarmEpisode) {
          console.log('There were no changes to the alarm!');
          return;
        }

        break;
      }
    }

    if (callRepetition === 0) {
      this.showLoading = true;
    }

    try {
      const prepareResponse = this.apiRequestsService.prepareAlarmUpdate(data.alarmId, data.alarmSeason, data.alarmEpisode);

      if (prepareResponse[0]) {
        prepareResponse[1].subscribe(
          response => {

            snackBar.openFromComponent(AlarmUpdatedSnackBarComponent, {
              duration: 5000
            });

            this.alarms.next(Utilities.getAlarmsWithRegexList(response['alarm_list']));

            this.showLoading = false;
          },
          error => {
            snackBar.openFromComponent(AlarmFailedSnackBarComponent, {
              duration: 5000
            });

            this.showLoading = false;
          }
        );
      } else {
        prepareResponse[1].subscribe(
          response => {
            this.tokenService.saveAccessToken(response.token);
            this.createAlarm(data, snackBar, callRepetition + 1);
          },
          // Refresh token is no longer valid
          error => {
            if (error.error !== ACCESS_TOKEN_ERROR) {
              this.tokenService.deleteRefreshToken();
            }

            snackBar.openFromComponent(AlarmFailedSnackBarComponent, {
              duration: 5000
            });

            this.showLoading = false;
          }
        );
      }
    } catch (error) {
      if (error.message === this.apiRequestsService.ERROR_LOGIN) {
        snackBar.openFromComponent(AlarmFailedSnackBarComponent, {
          duration: 5000
        });
      } else {
        console.log(error);
      }

      this.showLoading = false;
    }
  }

  /**
   * Delete a alarm.
   *
   * @param alarmId the id of the alarm to delete.
   * @param snackBar the snackbar.
   * @param callRepetition the current call of this function.
   */
  public deleteAlarm(alarmId: number, snackBar: MatSnackBar, callRepetition: number) {
    if (callRepetition > 1) {
      console.log('There was an error, we were unable to register your alarm!');
      return;
    }

    if (callRepetition === 0) {
      this.showLoading = true;
    }

    try {
      const prepareResponse = this.apiRequestsService.prepareDeleteAlarm(alarmId);

      if (prepareResponse[0]) {
        prepareResponse[1].subscribe(response => {
          snackBar.openFromComponent(AlarmDeletedSnackBarComponent, {
            duration: 5000
          });

          this.alarms.next(Utilities.getAlarmsWithRegexList(response['alarm_list']));

          this.showLoading = false;
        });
      } else {
        prepareResponse[1].subscribe(
          response => {
            this.tokenService.saveAccessToken(response.token);
            this.deleteAlarm(alarmId, snackBar, callRepetition + 1);
          },
          // Refresh token is no longer valid
          error => {
            if (error.error !== ACCESS_TOKEN_ERROR) {
              this.tokenService.deleteRefreshToken();
            }

            snackBar.openFromComponent(AlarmFailedSnackBarComponent, {
              duration: 5000
            });

            this.showLoading = false;
          }
        );
      }
    } catch (error) {
      if (error.message === this.apiRequestsService.ERROR_LOGIN) {
        console.log('Failed to get list of alarms, login required!');
      }

      this.showLoading = false;
    }
  }

  /**
   * Clear the alarms variable.
   */
  public clearAlarms() {
    this.alarms.next([]);
  }
}
