import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { timeout } from 'rxjs/operators';
import { BooleanWrapper } from 'src/app/booleanWrapper';
import { CheckEmailSnackBarComponent } from 'src/app/components/snack-bar/check-email-snackbar-component';
import { GenericProblemSnackBarComponent } from 'src/app/components/snack-bar/generic-problem-snackbar-component';
import { REQUEST_TIMEOUT, SERVER_URL } from 'src/app/constants';
import { ManagerService } from '../manager/manager.service';
import { TokenService } from '../token/token.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private readonly USERS_URL = SERVER_URL + '/users';

  constructor(private manager: ManagerService,
              public tokenService: TokenService,
              private httpClient: HttpClient) { }

  /**
   * Prepare request for registering a new user.
   *
   * @param email email.
   * @param password password.
   */
  public prepareUserRegistration(email: string, password: string,
                                 snackBar: MatSnackBar,
                                 showLoading: BooleanWrapper,
                                 successCallback,
                                 caller): void {
    this.manager.startProcessing(showLoading);

    const response$ = this.httpClient.post(this.USERS_URL, { email, password });

    response$.pipe(timeout(REQUEST_TIMEOUT)).subscribe(
      response => {
        successCallback(caller);

        snackBar.openFromComponent(CheckEmailSnackBarComponent, {
          duration: 5000,
        });

        this.manager.stopProcessing(showLoading);
      },
      error => {
        snackBar.openFromComponent(GenericProblemSnackBarComponent, {
          duration: 5000,
        });

        this.manager.stopProcessing(showLoading);
      }
    );
  }

  /**
   * Prepare request for verifying an account.
   *
   * @param verificationToken the verification token.
   */
  public prepareAccountVerification(verificationToken: string): Observable<any> {
    return this.httpClient.put(this.USERS_URL, { verification_token: verificationToken });
  }

  /**
   * Prepare request for deleting an account.
   *
   * @param deletionToken the deletion token.
   */
  public prepareAccountDeletion(deletionToken: string): Observable<any> {
    return this.httpClient.delete(this.USERS_URL, { params: { deletion_token: deletionToken } });
  }

  /**
   * Prepare request for changing something in an account.
   *
   * @param changeToken the change token.
   */
  public prepareAccountChange(changeToken: string): Observable<any> {
    return this.httpClient.put(this.USERS_URL, { change_token: changeToken }, {});
  }

  /**
   * Prepare request for fixing the email.
   *
   * @param currentEmail current email.
   * @param newEmail new email.
   */
  public prepareFixEmail(currentEmail: string, newEmail: string): Observable<any> {
    return this.httpClient.put(this.USERS_URL, { current_email: currentEmail, new_email: newEmail }, {});
  }
}
