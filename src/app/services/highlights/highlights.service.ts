import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Highlight } from 'src/app/structs/Highlight';
import { ApiRequestsService } from '../api-requests/api-requests.service';
import { add, sub } from 'date-fns'

@Injectable({
  providedIn: 'root'
})
export class HighlightsService {
  private showLoading: boolean[];
  private calculations: number;
    
  private currentWeekYear: number;
  private currentWeek: number;

  public scoreHighlights = new BehaviorSubject<Highlight[]>(null);
  public newHighlights = new BehaviorSubject<Highlight[]>(null);

  constructor(private apiRequestsService: ApiRequestsService) { }

  /**
   * Get all needed highlights.
   *
   * @param showLoading an array of booleans for when to show loading.
   * @returns all needed highlights.
   */
  public getHighlights(showLoading: boolean[]): void {
    // Check if this was already performed
    if (this.scoreHighlights.getValue() != null) {
      return;
    }

    // Initialize variables
    this.scoreHighlights.next([null, null, null]);
    this.newHighlights.next([null, null, null]);

    this.calculations = 3;
    this.showLoading = showLoading;

    for (var sl of this.showLoading) {
      sl = true;
    }

    // Get the current week  
    var today = new Date();
    var startOfWeek = sub(today, {days: today.getDate()});
      
    this.currentWeekYear = startOfWeek.getFullYear();
    this.currentWeek = this.iso8601WeekNo(startOfWeek);
      
    this.getWeeklyHighlights(this.currentWeekYear, this.currentWeek);
      
    // Delete the stored data for the (current week - 2)
    var startOfWeekToDelete = sub(startOfWeek, {days: 14});
      
    var weekToDeleteYear = startOfWeekToDelete.getFullYear();
    var weekToDelete = this.iso8601WeekNo(startOfWeekToDelete);
      
    localStorage.removeItem('highlights' + weekToDeleteYear.toString() + weekToDelete.toString());
      
    // Get the previous week
    var startOfPreviousWeek = sub(startOfWeek, {days: 7});
      
    var previousWeekYear = startOfPreviousWeek.getFullYear();
    var previousWeek = this.iso8601WeekNo(startOfPreviousWeek);
      
    this.getWeeklyHighlights(previousWeekYear, previousWeek);
      
    // Get the next week
    var startOfNextWeek = add(startOfWeek, {days: 7});
      
    var nextWeekYear = startOfNextWeek.getFullYear();
    var nextWeek = this.iso8601WeekNo(startOfNextWeek);
      
    this.getWeeklyHighlights(nextWeekYear, nextWeek);
  }

  /**
   * Get a week's corresponding highlights.
   *
   * @param year the year.
   * @param week the week.
   */
  private getWeeklyHighlights(year: number, week: number): void {
    // Check if the results have already been stored in the cookies
    const weeklyHightligts = localStorage.getItem('highlights' + year.toString() + week.toString());

    if (weeklyHightligts != null) {
      this.updateHighlights(JSON.parse(weeklyHightligts));
    } else {
      // If not, they need to be obtained from the server
      this.getHighlightsFromServer(year, week);
    }
  }

  /**
   * Make the request to get the highlights for a given week.
   *
   * @param year the year.
   * @param week the week.
   */
  private getHighlightsFromServer(year: number, week: number): void {
    try {
      const prepareResponse = this.apiRequestsService.prepareCallToHighlights(year, week);

      prepareResponse.subscribe(
        response => {
          if (response.highlight_list.length != 0) {
            // Store the response
            localStorage.setItem('highlights' + year.toString() + week.toString(), JSON.stringify(response.highlight_list));
          }

          this.updateHighlights(response.highlight_list);
        },
        error => {
          this.updateHighlights([]);
        }
      );
    } catch (error) {
      console.log(error);
      this.updateHighlights([]);
    }
  }

  /**
   * Do everything related to the update of the list of highlights.
   *
   * @param highlights the list of highlights.
   */
  private updateHighlights(highlights: Highlight[]): void {
    if (highlights != null && highlights.length > 0) {
      const scoreHighlights = this.scoreHighlights.getValue();
      const newHighlights = this.newHighlights.getValue();

      // Assign the highlights to the correct variables
      for (const h of highlights) {
        if (h.key == "SCORE") {
            if (h.year < this.currentWeekYear || (h.year == this.currentWeekYear && h.week < this.currentWeek)) {
            scoreHighlights[0] = h;
          } else if (h.week == this.currentWeek) {
            scoreHighlights[1] = h;
          } else {
            scoreHighlights[2] = h;
          }
        } else {
          if (h.year < this.currentWeekYear || (h.year == this.currentWeekYear && h.week < this.currentWeek)) {
            newHighlights[0] = h;
          } else if (h.week == this.currentWeek) {
            newHighlights[1] = h;
          } else {
            newHighlights[2] = h;
          }
        }
      }

      this.scoreHighlights.next(scoreHighlights);
      this.newHighlights.next(newHighlights);
    }

    this.calculations--;

    // If there are no more calculations left, loading is complete
    if (this.calculations == 0) {
      for (var showLoading of this.showLoading) {
        showLoading = false;
      }
    }
  }

  /**
   * Calculate the current week number, according to the ISO 8601.
   * Source: https://www.w3resource.com/javascript-exercises/javascript-date-exercise-24.php
  */
  private iso8601WeekNo(dt) {
    var tdt = new Date(dt.valueOf());
    var dayn = (dt.getDay() + 6) % 7;

    tdt.setDate(tdt.getDate() - dayn + 3);

    var firstThursday = tdt.valueOf();
    tdt.setMonth(0, 1);

    if (tdt.getDay() !== 4) {
      tdt.setMonth(0, 1 + ((4 - tdt.getDay()) + 7) % 7);
    }

    return 1 + Math.ceil((firstThursday - tdt.valueOf()) / 604800000);
  }
}
