import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject } from 'rxjs';
import { ReminderCreatedSnackBarComponent } from 'src/app/components/snack-bar/reminder-created-snackbar-component';
import { ReminderDeletedSnackBarComponent } from 'src/app/components/snack-bar/reminder-deleted-snackbar-component';
import { ReminderFailedSnackBarComponent } from 'src/app/components/snack-bar/reminder-failed-snackbar-component';
import { ReminderUpdatedSnackBarComponent } from 'src/app/components/snack-bar/reminder-updated-snackbar-component';
import { ACCESS_TOKEN_ERROR } from 'src/app/constants';
import { Reminder } from 'src/app/structs/Reminder';
import { ReminderModalData } from 'src/app/structs/ReminderModalData';
import { ApiRequestsService } from '../api-requests/api-requests.service';
import { TokenService } from '../token/token.service';

@Injectable({
  providedIn: 'root'
})
export class ReminderService {
  public reminders = new BehaviorSubject<Reminder[]>([]);
  public showLoading = true;

  constructor(private apiRequestsService: ApiRequestsService,
    private tokenService: TokenService) {
    // This obtains the current list of alarms, if the user is logged in
    if (tokenService.validateRefreshToken()) {
      this.getReminderList();
    }
  }

  /**
   * Get the list of reminders of the user.
   */
  public getReminderList() {
    return this.getReminders(0);
  }

  /**
   * Get the list of reminders of the user.
   *
   * @param callRepetition the current call of this function.
   */
  private getReminders(callRepetition: number) {
    if (callRepetition > 1) {
      console.log('There was an error, we were unable to obtain the list of reminders!');
      return;
    }

    if (callRepetition === 0) {
      this.showLoading = true;
    }

    try {
      const prepareResponse = this.apiRequestsService.prepareGetReminders();

      if (prepareResponse[0]) {
        prepareResponse[1].subscribe(response => {
          this.reminders.next(response['reminder_list']);

          this.showLoading = false;
        });
      } else {
        prepareResponse[1].subscribe(
          response => {
            this.tokenService.saveAccessToken(response['token']);
            this.getReminders(callRepetition + 1);
          },
          // Refresh token is no longer valid
          error => {
            if (error.error !== ACCESS_TOKEN_ERROR) {
              this.tokenService.deleteRefreshToken();
            }

            this.showLoading = false;
          }
        );
      }
    } catch (error) {
      if (error.message === this.apiRequestsService.ERROR_LOGIN) {
        /*
          Since this is only performed at the beginning, there's no need to warn the user.
        */
      }

      this.showLoading = false;
    }
  }

  /**
   * Create a reminder associated with the user.
   *
   * @param data the data.
   * @param snackBar the snackbar.
   * @param callRepetition the current call of this function.
   */
  public createReminder(data: ReminderModalData, snackBar: MatSnackBar, callRepetition: number) {
    if (callRepetition > 1) {
      console.log('There was an error, we were unable to register your reminder!');
      return;
    }

    // If a reminder already exists, use update
    if (data.reminderId != null) {
      return;
    }

    if (callRepetition === 0) {
      this.showLoading = true;
    }

    let prepareResponse;

    try {
      prepareResponse = this.apiRequestsService.prepareReminderRegistration(data.sessionId, data.reminderAnticipationHours);
    } catch (error) {
      if (error.message === this.apiRequestsService.ERROR_LOGIN) {
        console.log('Failed to get list of reminders, login required!');
      }

      this.showLoading = false;
      return;
    }

    // If the access token is still valid
    if (prepareResponse[0]) {
      prepareResponse[1].subscribe(
        response => {
          snackBar.openFromComponent(ReminderCreatedSnackBarComponent, {
            duration: 5000
          });

          this.reminders.next(response['reminder_list']);
          this.showLoading = false;
        },
        error => {
          snackBar.openFromComponent(ReminderFailedSnackBarComponent, {
            duration: 5000
          });

          this.showLoading = false;
        }
      );
    } else { // If a new access token is needed
      prepareResponse[1].subscribe(
        response => {
          this.tokenService.saveAccessToken(response.token);
          this.createReminder(data, snackBar, callRepetition + 1);
        },
        // Refresh token is no longer valid
        error => {
          if (error.error !== ACCESS_TOKEN_ERROR) {
            this.tokenService.deleteRefreshToken();
          }

          snackBar.openFromComponent(ReminderFailedSnackBarComponent, {
            duration: 5000
          });

          this.showLoading = false;
        }
      );
    }
  }

  /**
   * Update a reminder associated with the user.
   *
   * @param data the data.
   * @param snackBar the snackbar.
   * @param callRepetition the current call of this function.
   */
  public updateReminder(data: ReminderModalData, snackBar: MatSnackBar, callRepetition: number) {
    if (callRepetition > 1) {
      console.log('There was an error, we were unable to update your reminder!');
      return;
    }

    // If a reminder does not exist, use create
    if (data.reminderId == null) {
      return;
    }

    for (let i = 0, size = this.reminders.getValue().length; i < size; i++) {
      const r = this.reminders.getValue()[i];

      if (r.id === data.reminderId) {
        if (r.anticipation_minutes / 60 === data.reminderAnticipationHours) {
          console.log('There were no changes to the reminder!');
          return;
        }

        break;
      }
    }

    if (callRepetition === 0) {
      this.showLoading = true;
    }

    try {
      const prepareResponse = this.apiRequestsService.prepareReminderUpdate(data.reminderId, data.reminderAnticipationHours);

      if (prepareResponse[0]) {
        prepareResponse[1].subscribe(
          response => {

            snackBar.openFromComponent(ReminderUpdatedSnackBarComponent, {
              duration: 5000
            });

            this.reminders.next(response['reminder_list']);

            this.showLoading = false;
          },
          error => {
            snackBar.openFromComponent(ReminderFailedSnackBarComponent, {
              duration: 5000
            });

            this.showLoading = false;
          }
        );
      } else {
        prepareResponse[1].subscribe(
          response => {
            this.tokenService.saveAccessToken(response.token);
            this.createReminder(data, snackBar, callRepetition + 1);
          },
          // Refresh token is no longer valid
          error => {
            if (error.error !== ACCESS_TOKEN_ERROR) {
              this.tokenService.deleteRefreshToken();
            }

            snackBar.openFromComponent(ReminderFailedSnackBarComponent, {
              duration: 5000
            });

            this.showLoading = false;
          }
        );
      }
    } catch (error) {
      if (error.message === this.apiRequestsService.ERROR_LOGIN) {
        snackBar.openFromComponent(ReminderFailedSnackBarComponent, {
          duration: 5000
        });
      } else {
        console.log(error);
      }

      this.showLoading = false;
    }
  }

  /**
   * Delete a reminder.
   *
   * @param reminderId the id of the reminder to delete.
   * @param snackBar the snackbar.
   * @param callRepetition the current call of this function.
   */
  public deleteReminder(reminderId: number, snackBar: MatSnackBar, callRepetition: number) {
    if (callRepetition > 1) {
      console.log('There was an error, we were unable to delete your reminder!');
      return;
    }

    if (callRepetition === 0) {
      this.showLoading = true;
    }

    try {
      const prepareResponse = this.apiRequestsService.prepareDeleteReminder(reminderId);

      if (prepareResponse[0]) {
        prepareResponse[1].subscribe(response => {
          snackBar.openFromComponent(ReminderDeletedSnackBarComponent, {
            duration: 5000
          });

          this.reminders.next(response['reminder_list']);

          this.showLoading = false;
        });
      } else {
        prepareResponse[1].subscribe(
          response => {
            this.tokenService.saveAccessToken(response.token);
            this.deleteReminder(reminderId, snackBar, callRepetition + 1);
          },
          // Refresh token is no longer valid
          error => {
            if (error.error !== ACCESS_TOKEN_ERROR) {
              this.tokenService.deleteRefreshToken();
            }

            snackBar.openFromComponent(ReminderFailedSnackBarComponent, {
              duration: 5000
            });

            this.showLoading = false;
          }
        );
      }
    } catch (error) {
      if (error.message === this.apiRequestsService.ERROR_LOGIN) {
        console.log('Failed to get list of reminder, login required!');
      }

      this.showLoading = false;
    }
  }

  /**
   * Clear the reminders variable.
   */
  public clearReminders() {
    this.reminders.next([]);
  }
}
