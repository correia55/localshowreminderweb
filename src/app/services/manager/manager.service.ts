import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { BooleanWrapper } from 'src/app/booleanWrapper';
import { REQUEST_LOADING_THRESHOLD } from 'src/app/constants';
import { ProcessingPhase } from 'src/app/processingPhase';

@Injectable({
  providedIn: 'root'
})
export class ManagerService {
  public isProcessing$: BehaviorSubject<ProcessingPhase>;

  constructor() {
    this.isProcessing$ = new BehaviorSubject<ProcessingPhase>(ProcessingPhase.Nothing);
  }

  /**
   * Start some processing.
   * @param showLoading the show loading for some component.
   */
  public startProcessing(showLoading: BooleanWrapper) {
    // Set the processing phase to starting
    this.isProcessing$.next(ProcessingPhase.Starting);

    // Wait a while and, if the processing did not finish in the meantime
    // set the processing phase to processing
    setTimeout(() => {
      if (this.isProcessing$.getValue() !== ProcessingPhase.Nothing) {
        this.isProcessing$.next(ProcessingPhase.Processing);

        if (showLoading) {
          showLoading.bool = true;
        }
      }
    }, REQUEST_LOADING_THRESHOLD);
  }

  /**
   * Stop some processing.
   * @param showLoading the show loading for some component.
   */
  public stopProcessing(showLoading: BooleanWrapper) {
    this.isProcessing$.next(ProcessingPhase.Nothing);

    if (showLoading) {
      showLoading.bool = false;
    }
  }
}
