import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { TokenService } from '../token/token.service';
import { Observable } from 'rxjs/internal/Observable';
import { DataType } from 'src/app/structs/AlarmModalData';
import { environment } from 'src/environments/environment';
import { SERVER_URL } from 'src/app/constants';

@Injectable({
  providedIn: 'root'
})
export class ApiRequestsService {
  private readonly LOGIN_URL = SERVER_URL + '/login';
  private readonly EXTERNAL_LOGIN_URL = SERVER_URL + '/external-login';
  private readonly LOGOUT_URL = SERVER_URL + '/logout';
  private readonly RECOVER_PASSWORD_URL = SERVER_URL + '/recover-password';
  private readonly SEND_EMAIL_URL = SERVER_URL + '/send-email';
  private readonly SEND_CHANGE_EMAIL_URL = SERVER_URL + '/send-change-email';
  private readonly SEND_PASSWORD_RECOVERY_EMAIL_URL = SERVER_URL + '/send-password-recovery-email';

  private readonly ALARMS_URL = SERVER_URL + '/alarms';
  private readonly REMINDERS_URL = SERVER_URL + '/reminders';
  private readonly ACCESS_URL = SERVER_URL + '/access';
  private readonly SHOWS_URL = SERVER_URL + '/shows';
  private readonly LOCAL_SHOWS_URL = SERVER_URL + '/local-shows';
  private readonly USERS_BA_SETTINGS_URL = SERVER_URL + '/users-ba-settings';
  private readonly HIGHLIGHTS_URL = SERVER_URL + '/highlights';

  private readonly PARAMS = 'params';

  public readonly ERROR_LOGIN = 'loginRequired';

  language: string;

  constructor(private httpClient: HttpClient, private tokenService: TokenService) {
    this.language = environment.language;
  }

  /**
   * Create and return the authorization headers with the access token.
   */
  public getAuthorizationHeaders() {
    return { headers: new HttpHeaders({ Authorization: 'Bearer ' + this.tokenService.getAccessToken() }) };
  }

  /**
   * Prepare request for logging in an user.
   *
   * @param email email.
   * @param password password.
   */
  public prepareUserLogin(email: string, password: string): Observable<any> {
    return this.httpClient.post(this.LOGIN_URL, {}, {
      headers: new HttpHeaders({ Authorization: 'Basic ' + btoa(email + ':' + password) })
    });
  }

  /**
   * Prepare request for logging in an user, using an external source.
   *
   * @param email email.
   * @param password password.
   */
  public prepareUserExternalLogin(token: string, source: string): Observable<any> {
    return this.httpClient.post(this.EXTERNAL_LOGIN_URL, { external_token: token, source: source });
  }

  /**
   * Prepare request for recovering the password.
   *
   * @param email email.
   */
  public prepareSendPasswordRecoveryEmail(email: string): Observable<any> {
    return this.httpClient.post(this.SEND_PASSWORD_RECOVERY_EMAIL_URL, { email });
  }

  /**
   * Prepare request for changing the password after a password recovery.
   *
   * @param email email.
   */
  public preparePasswordRecovery(token: string, password: string): Observable<any> {
    return this.httpClient.put(this.RECOVER_PASSWORD_URL, { token, password });
  }

  /**
   * Prepare request for logging out an user.
   */
  public prepareUserLogout(): Observable<any> {
    return this.httpClient.post(this.LOGOUT_URL,
      { refresh_token: this.tokenService.getRefreshToken() });
  }

  /**
   * Prepare request for searching db.
   *
   * @param searchText the search text.
   */
  public prepareSearchDB(searchText: string): [boolean, Observable<any>] {
    const isLoggedIn = this.tokenService.validateRefreshToken();
    const isAccessTokenReady = this.tokenService.validateAccessToken();

    // If logged in
    if (isLoggedIn) {
      // If access token is ready send request
      if (isAccessTokenReady) {
        const options = this.getAuthorizationHeaders();
        options[this.PARAMS] = { search_text: searchText, language: this.language };

        return [true, this.httpClient.get(this.SHOWS_URL, options)];
      } else { // Otherwise send request for new access token
        return [false, this.prepareGetNewAccessToken()];
      }
    } else {
      return [true, this.httpClient.get(this.SHOWS_URL, { params: { search_text: searchText, language: this.language } })];
    }
  }

  /**
   * Prepare request for searching listings.
   *
   * @param searchText the search text.
   */
  public prepareSearchListings(searchText: string): [boolean, Observable<any>] {
    const isLoggedIn = this.tokenService.validateRefreshToken();
    const isAccessTokenReady = this.tokenService.validateAccessToken();

    // If logged in
    if (isLoggedIn) {
      // If access token is ready send request
      if (isAccessTokenReady) {
        const options = this.getAuthorizationHeaders();
        options[this.PARAMS] = { search_text: searchText };

        return [true, this.httpClient.get(this.LOCAL_SHOWS_URL, options)];
      } else { // Otherwise send request for new access token
        return [false, this.prepareGetNewAccessToken()];
      }
    } else { // If not logged in, send request without authorization headers
      return [true, this.httpClient.get(this.LOCAL_SHOWS_URL, { params: { search_text: searchText } })];
    }
  }

  /**
   * Prepare request for searching listings.
   *
   * @param searchText the search text.
   */
  public prepareSearchListingsById(showId: number, isMovie: boolean): [boolean, Observable<any>] {
    const isLoggedIn = this.tokenService.validateRefreshToken();
    const isAccessTokenReady = this.tokenService.validateAccessToken();

    // If logged in
    if (isLoggedIn) {
      // If access token is ready send request
      if (isAccessTokenReady) {
        const options = this.getAuthorizationHeaders();
        options[this.PARAMS] = { show_id: showId, is_movie: isMovie };

        return [true, this.httpClient.get(this.LOCAL_SHOWS_URL, options)];
      } else { // Otherwise send request for new access token
        return [false, this.prepareGetNewAccessToken()];
      }
    } else { // If not logged in, send request without authorization headers
      return [true, this.httpClient.get(this.LOCAL_SHOWS_URL, { params: { show_id: showId.toString(), is_movie: isMovie.toString() } })];
    }
  }

  /**
   * Prepare the request to get a new access token.
   */
  public prepareGetNewAccessToken() {
    if (!this.tokenService.validateRefreshToken()) {
      throw new Error(this.ERROR_LOGIN);
    }

    return this.httpClient.post(this.ACCESS_URL, { refresh_token: this.tokenService.getRefreshToken() });
  }

  /**
   * Prepare request for registring a alarm.
   *
   * @param showName the name of the show.
   * @param isMovie true if it is a movie.
   * @param alarmType the type of alarm.
   * @param showSeason the season.
   * @param showEpisode the episode.
   */
  public prepareAlarmRegistration(showName: string, isMovie: boolean,
    alarmType: string, showSeason: number,
    showEpisode: number, traktId: string,
    showLanguage: string): [boolean, Observable<any>] {
    if (alarmType !== DataType.LISTINGS.toString() && alarmType !== DataType.DB.toString()) {
      throw new Error('invalidShowType');
    }

    const isAccessTokenReady = this.tokenService.validateAccessToken();

    let response$: Observable<any>;

    // I don't understand why but even though is_movie is a boolean, I need to
    // use isMovie ? true : false for it to work
    if (isAccessTokenReady) {
      const request = {
        show_name: showName, is_movie: isMovie ? true : false,
        type: alarmType, trakt_id: traktId, show_language: showLanguage
      };

      if (!isMovie) {
        request['show_season'] = showSeason;
        request['show_episode'] = showEpisode;
      }

      response$ = this.httpClient.post(this.ALARMS_URL, request,
        this.getAuthorizationHeaders());
    } else {
      response$ = this.prepareGetNewAccessToken();
    }

    return [isAccessTokenReady, response$];
  }

  /**
   * Prepare request for updating a alarm.
   *
   * @param alarmId the id of the alarm.
   * @param showSeason the season.
   * @param showEpisode the episode.
   */
  public prepareAlarmUpdate(alarmId: number, showSeason: number, showEpisode: number): [boolean, Observable<any>] {
    const isAccessTokenReady = this.tokenService.validateAccessToken();

    let response$: Observable<any>;

    // I don't understand why but even though is_movie is a boolean, I need to
    // use isMovie ? true : false for it to work
    if (isAccessTokenReady) {
      const request = { 'alarm_id': alarmId, 'show_season': showSeason, 'show_episode': showEpisode };

      response$ = this.httpClient.put(this.ALARMS_URL, request,
        this.getAuthorizationHeaders());
    } else {
      response$ = this.prepareGetNewAccessToken();
    }

    return [isAccessTokenReady, response$];
  }

  /**
   * Prepare the request to get the list of alarms.
   */
  public prepareGetAlarms(): [boolean, Observable<any>] {
    const isAccessTokenReady = this.tokenService.validateAccessToken();

    let response$: Observable<any>;

    if (isAccessTokenReady) {
      response$ = this.httpClient.get(this.ALARMS_URL, this.getAuthorizationHeaders());
    } else {
      response$ = this.prepareGetNewAccessToken();
    }

    return [isAccessTokenReady, response$];
  }

  /**
   * Prepare the request to delete a alarm.
   * @param alarmId the id of the alarm to delete.
   */
  public prepareDeleteAlarm(alarmId: number): [boolean, Observable<any>] {
    const isAccessTokenReady = this.tokenService.validateAccessToken();

    let response$: Observable<any>;

    if (isAccessTokenReady) {
      const options = this.getAuthorizationHeaders();
      options[this.PARAMS] = { alarm_id: alarmId };

      response$ = this.httpClient.delete(this.ALARMS_URL, options);
    } else {
      response$ = this.prepareGetNewAccessToken();
    }

    return [isAccessTokenReady, response$];
  }

  /**
   * Prepare request for registring a reminder.
   *
   * @param sessionId the id of the session.
   * @param anticipationHours the anticipation hours for the reminder.
   */
  public prepareReminderRegistration(sessionId: number, anticipationHours: number): [boolean, Observable<any>] {
    const isAccessTokenReady = this.tokenService.validateAccessToken();

    let response$: Observable<any>;

    // I don't understand why but even though is_movie is a boolean, I need to
    // use isMovie ? true : false for it to work
    if (isAccessTokenReady) {
      const request = {
        show_session_id: sessionId, anticipation_minutes: anticipationHours * 60
      };

      response$ = this.httpClient.post(this.REMINDERS_URL, request,
        this.getAuthorizationHeaders());
    } else {
      response$ = this.prepareGetNewAccessToken();
    }

    return [isAccessTokenReady, response$];
  }

  /**
   * Prepare the request to get the list of reminders.
   */
  public prepareGetReminders(): [boolean, Observable<any>] {
    const isAccessTokenReady = this.tokenService.validateAccessToken();

    let response$: Observable<any>;

    if (isAccessTokenReady) {
      response$ = this.httpClient.get(this.REMINDERS_URL, this.getAuthorizationHeaders());
    } else {
      response$ = this.prepareGetNewAccessToken();
    }

    return [isAccessTokenReady, response$];
  }

  /**
   * Prepare request for updating a reminder.
   *
   * @param reminderId the id of the reminder.
   * @param showSeason the season.
   * @param showEpisode the episode.
   */
  public prepareReminderUpdate(reminderId: number, anticipation_hours: number): [boolean, Observable<any>] {
    const isAccessTokenReady = this.tokenService.validateAccessToken();

    let response$: Observable<any>;

    // I don't understand why but even though is_movie is a boolean, I need to
    // use isMovie ? true : false for it to work
    if (isAccessTokenReady) {
      const request = { 'reminder_id': reminderId, 'anticipation_minutes': anticipation_hours * 60 };

      response$ = this.httpClient.put(this.REMINDERS_URL, request,
        this.getAuthorizationHeaders());
    } else {
      response$ = this.prepareGetNewAccessToken();
    }

    return [isAccessTokenReady, response$];
  }

  /**
   * Prepare the request to delete a reminder.
   * @param reminderId the id of the reminder to delete.
   */
  public prepareDeleteReminder(reminderId: number): [boolean, Observable<any>] {
    const isAccessTokenReady = this.tokenService.validateAccessToken();

    let response$: Observable<any>;

    if (isAccessTokenReady) {
      const options = this.getAuthorizationHeaders();
      options[this.PARAMS] = { reminder_id: reminderId };

      response$ = this.httpClient.delete(this.REMINDERS_URL, options);
    } else {
      response$ = this.prepareGetNewAccessToken();
    }

    return [isAccessTokenReady, response$];
  }

  /**
   * Prepare request for deleting an account.
   */
  public prepareRequestAccountDeletion(): [boolean, Observable<any>] {
    const isAccessTokenReady = this.tokenService.validateAccessToken();

    let response$: Observable<any>;

    if (isAccessTokenReady) {
      response$ = this.httpClient.post(this.SEND_EMAIL_URL, { type: 'DELETION' }, this.getAuthorizationHeaders());
    } else {
      response$ = this.prepareGetNewAccessToken();
    }

    return [isAccessTokenReady, response$];
  }

  /**
   * Prepare request for changing the email.
   */
  public prepareRequestEmailChange(): [boolean, Observable<any>] {
    const isAccessTokenReady = this.tokenService.validateAccessToken();

    let response$: Observable<any>;

    if (isAccessTokenReady) {
      response$ = this.httpClient.post(this.SEND_EMAIL_URL, { type: 'CHANGE_OLD' }, this.getAuthorizationHeaders());
    } else {
      response$ = this.prepareGetNewAccessToken();
    }

    return [isAccessTokenReady, response$];
  }

  /**
   * Prepare request for changing to the new email.
   */
  public prepareNewEmailChange(changeToken: string, newEmail: string): [boolean, Observable<any>] {
    const isAccessTokenReady = this.tokenService.validateAccessToken();

    let response$: Observable<any>;

    if (isAccessTokenReady) {
      response$ = this.httpClient.post(this.SEND_CHANGE_EMAIL_URL,
        { new_email: newEmail, change_token: changeToken },
        this.getAuthorizationHeaders());
    } else {
      response$ = this.prepareGetNewAccessToken();
    }

    return [isAccessTokenReady, response$];
  }

  /**
   * Prepare request for changing the password.
   */
  public preparePasswordChange(password: string, newPassword: string): [boolean, Observable<any>] {
    const isAccessTokenReady = this.tokenService.validateAccessToken();

    let response$: Observable<any>;

    if (isAccessTokenReady) {
      response$ = this.httpClient.put(this.USERS_BA_SETTINGS_URL,
        { password, new_password: newPassword },
        this.getAuthorizationHeaders());
    } else {
      response$ = this.prepareGetNewAccessToken();
    }

    return [isAccessTokenReady, response$];
  }

  /**
   * Prepare request for getting the list of highlights.
   */
  public prepareCallToHighlights(year: number, week: number): Observable<any> {
    return this.httpClient.get(this.HIGHLIGHTS_URL, { params: { "year": year.toString(), "week": week.toString() } });
  }
}
