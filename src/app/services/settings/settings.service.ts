import { Injectable } from '@angular/core';
import { TokenService } from '../token/token.service';
import { ApiRequestsService } from '../api-requests/api-requests.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GenericProblemSnackBarComponent } from '../../components/snack-bar/generic-problem-snackbar-component';
import { SessionExpiredSnackBarComponent } from '../../components/snack-bar/session-expired-snackbar-component';
import { CheckEmailSnackBarComponent } from 'src/app/components/snack-bar/check-email-snackbar-component';
import { timeout } from 'rxjs/operators';
import { ACCESS_TOKEN_ERROR, REQUEST_TIMEOUT, SERVER_URL } from 'src/app/constants';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  private readonly USERS_SETTINGS_URL = SERVER_URL + '/users-settings';

  private readonly SERVER_SETTING_LANGUAGE = 'language';
  private readonly SERVER_SETTING_ADULT = 'include_adult_channels';
  private readonly SERVER_SETTING_EXCLUDED_CHANNEL_LIST = 'excluded_channel_list';

  public language: string;
  public includeAdultsChannels: boolean;
  public excludedChannelSet: Set<number>;

  public showLoading$: BehaviorSubject<boolean>;

  constructor(private httpClient: HttpClient,
    private apiRequestsService: ApiRequestsService,
    private tokenService: TokenService) {
    this.showLoading$ = new BehaviorSubject<boolean>(false);

    // This obtains the current list of settings, if the user is logged in
    if (tokenService.validateRefreshToken()) {
      this.getSettingsList();
    }
  }

  /**
   * Get the user's language of preference.
   */
  public getUserLanguage() {
    return this.language;
  }

  /**
   * Get the user's adult setting.
   */
  public getUserAdultSetting() {
    return this.includeAdultsChannels;
  }

  /**
   * Get the user's excluded channels list.
   */
  public getUserExcludedChannelListSetting() {
    return this.excludedChannelSet;
  }

  /**
   * Get the list of settings of the user.
   */
  public getSettingsList() {
    this.getSettingsRequest(0);
  }

  /**
   * Get the list of settings of the user.
   *
   * @param callRepetition the current call of this function.
   */
  private getSettingsRequest(callRepetition: number) {
    if (callRepetition > 1) {
      console.log('There was an error, we were unable to get your settings!');
      return;
    }

    if (callRepetition === 0) {
      this.showLoading$.next(true);
    }

    let prepareResponse: [boolean, Observable<any>];

    try {
      prepareResponse = this.prepareGetsettings();
    } catch (error) {
      if (error.message === this.apiRequestsService.ERROR_LOGIN) {
        // Since this is only performed at the beginning, there's no need to warn the user.

        this.showLoading$.next(false);
        return;
      }
    }

    if (prepareResponse[0]) {
      prepareResponse[1].pipe(timeout(REQUEST_TIMEOUT)).subscribe(
        response => { this.saveSettings(response); },
        error => { },
        () => { this.showLoading$.next(false); }
      );
    } else {
      prepareResponse[1].pipe(timeout(REQUEST_TIMEOUT)).subscribe(
        response => {
          this.tokenService.saveAccessToken(response[TokenService.RESPONSE_TOKEN]);
          this.getSettingsRequest(callRepetition + 1);
        },
        error => {
          // Refresh token is no longer valid
          if (error.error !== ACCESS_TOKEN_ERROR) {
            this.tokenService.deleteRefreshToken();
          }

          this.showLoading$.next(false);
        }
      );
    }
  }

  /**
   * Check if there are changes between the current and the new settings.
   * @param includeAdultsChannels the adult setting.
   * @param language the new language.
   * @param excludedChannelSet the set with the excluded channels.
   * @returns True if there are changes between the current and the new settings.
   */
  public hasChanges(includeAdultsChannels: boolean, language: string, excludedChannelSet: Set<number>): boolean {
    return !(includeAdultsChannels == this.includeAdultsChannels
      && language == this.language
      // To compare the two sets
      && Array.from(excludedChannelSet).sort().join() === Array.from(this.excludedChannelSet).sort().join());
  }

  /**
   * Update the user's language.
   * @param includeAdultsChannels the adult setting.
   * @param language the new language.
   * @param excludedChannelSet the set with the excluded channels.
   * @param snackBar the snack bar of the calling component.
   */
  public updateUserSettings(includeAdultsChannels: boolean, language: string, excludedChannelSet: Set<number>, snackBar: MatSnackBar) {
    // Check if there are any changes
    if (!this.hasChanges(includeAdultsChannels, language, excludedChannelSet)) {
      return;
    }

    const payload = this.createSettingsChangePayload(language, includeAdultsChannels, excludedChannelSet);
    this.updateUserSettingsRequests(payload, snackBar, 0);
  }

  /**
   * Update the user's settings.
   * @param callRepetition the current call to this function.
   * @param snackBar the snack bar of the calling component.
   */
  private updateUserSettingsRequests(payload: {}, snackBar: MatSnackBar, callRepetition: number) {
    if (callRepetition > 1) {
      console.log('There was an error, while deleting the account!');
      return;
    }

    if (callRepetition === 0) {
      this.showLoading$.next(true);
    }

    let prepareResponse: [boolean, Observable<any>];

    try {
      prepareResponse = this.prepareSettingsChange(payload);
    } catch (error) {
      if (error.message === this.apiRequestsService.ERROR_LOGIN) {
        snackBar.openFromComponent(SessionExpiredSnackBarComponent, {
          duration: 5000,
        });

        this.showLoading$.next(false);
        return;
      }
    }

    // If we have a valid access token
    if (prepareResponse[0]) {
      prepareResponse[1].subscribe(
        response => { this.saveSettings(response); },
        error => {
          snackBar.openFromComponent(GenericProblemSnackBarComponent, {
            duration: 5000,
          });
        },
        () => { this.showLoading$.next(false); }
      );
    } else { // If we need a new access token
      prepareResponse[1].subscribe(
        response => {
          this.tokenService.saveAccessToken(response[TokenService.RESPONSE_TOKEN]);
          this.updateUserSettingsRequests(payload, snackBar, callRepetition + 1);
        },
        error => {
          // Refresh token is no longer valid
          if (error.error !== ACCESS_TOKEN_ERROR) {
            this.tokenService.deleteRefreshToken();
          }

          snackBar.openFromComponent(SessionExpiredSnackBarComponent, {
            duration: 5000,
          });

          this.showLoading$.next(false);
        }
      );
    }
  }

  /**
   * Make the request to change email.
   * @param snackBar the snack bar of the calling component.
   */
  public changeEmail(snackBar: MatSnackBar) {
    this.changeEmailRequests(snackBar, 0);
  }

  /**
   * Make the request to change email.
   * @param callRepetition the current call to this function.
   * @param snackBar the snack bar of the calling component.
   */
  private changeEmailRequests(snackBar: MatSnackBar, callRepetition: number) {
    if (callRepetition > 1) {
      console.log('There was an error, while changing the email!');
      return;
    }

    if (callRepetition === 0) {
      this.showLoading$.next(true);
    }

    let prepareResponse: [boolean, Observable<any>];

    try {
      prepareResponse = this.apiRequestsService.prepareRequestEmailChange();
    } catch (error) {
      if (error.message === this.apiRequestsService.ERROR_LOGIN) {
        snackBar.openFromComponent(SessionExpiredSnackBarComponent, {
          duration: 5000,
        });

        this.showLoading$.next(false);
        return;
      }
    }

    if (prepareResponse[0]) {
      prepareResponse[1].subscribe(
        response => {
          snackBar.openFromComponent(CheckEmailSnackBarComponent, {
            duration: 5000,
          });
        },
        error => {
          snackBar.openFromComponent(GenericProblemSnackBarComponent, {
            duration: 5000,
          });
        },
        () => { this.showLoading$.next(false); }
      );
    } else {
      prepareResponse[1].subscribe(
        response => {
          this.tokenService.saveAccessToken(response.token);
          this.changeEmailRequests(snackBar, callRepetition + 1);
        },
        error => {
          // Refresh token is no longer valid
          if (error.error !== ACCESS_TOKEN_ERROR) {
            this.tokenService.deleteRefreshToken();
          }

          snackBar.openFromComponent(GenericProblemSnackBarComponent, {
            duration: 5000,
          });

          this.showLoading$.next(false);
        }
      );
    }
  }

  /**
   * Make the request to delete the account.
   * @param snackBar the snack bar of the calling component.
   */
  public deleteAccount(snackBar: MatSnackBar) {
    this.deleteAccountRequests(snackBar, 0);
  }

  /**
   * Make the request to delete the account.
   * @param callRepetition the current call to this function.
   * @param snackBar the snack bar of the calling component.
   */
  private deleteAccountRequests(snackBar: MatSnackBar, callRepetition: number) {
    if (callRepetition > 1) {
      console.log('There was an error, while deleting the account!');
      return;
    }

    if (callRepetition === 0) {
      this.showLoading$.next(true);
    }
    let prepareResponse: [boolean, Observable<any>];

    try {
      prepareResponse = this.apiRequestsService.prepareRequestAccountDeletion();
    } catch (error) {
      if (error.message === this.apiRequestsService.ERROR_LOGIN) {
        snackBar.openFromComponent(SessionExpiredSnackBarComponent, {
          duration: 5000,
        });

        this.showLoading$.next(false);
        return;
      }
    }

    if (prepareResponse[0]) {
      prepareResponse[1].subscribe(
        response => {
          snackBar.openFromComponent(CheckEmailSnackBarComponent, {
            duration: 5000,
          });
        },
        error => {
          snackBar.openFromComponent(GenericProblemSnackBarComponent, {
            duration: 5000,
          });
        },
        () => { this.showLoading$.next(false); }
      );
    } else {
      prepareResponse[1].subscribe(
        response => {
          this.tokenService.saveAccessToken(response.token);
          this.deleteAccountRequests(snackBar, callRepetition + 1);
        },
        error => {
          // Refresh token is no longer valid
          if (error.error !== ACCESS_TOKEN_ERROR) {
            this.tokenService.deleteRefreshToken();
          }

          snackBar.openFromComponent(GenericProblemSnackBarComponent, {
            duration: 5000,
          });

          this.showLoading$.next(false);
        }
      );
    }
  }

  /**
   * Save the settings with the response from the server.
   * @param response the response from the server.
   */
  private saveSettings(response: object) {
    this.saveUserLanguage(response[this.SERVER_SETTING_LANGUAGE]);
    this.saveUserAdultSetting(response[this.SERVER_SETTING_ADULT]);
    this.saveUserExcludedChannelListSetting(response[this.SERVER_SETTING_EXCLUDED_CHANNEL_LIST]);
  }

  /**
   * Save the user's language setting.
   */
  private saveUserLanguage(language: string) {
    this.language = language;
  }

  /**
   * Save the user's adult setting.
   */
  private saveUserAdultSetting(adultSetting: boolean) {
    this.includeAdultsChannels = adultSetting;
  }

  /**
   * Save the user's adult setting.
   */
  private saveUserExcludedChannelListSetting(excludedChannelList: [number]) {
    this.excludedChannelSet = new Set(excludedChannelList);
  }

  /**
   * Prepare the request to get the list of settings.
   */
  public prepareGetsettings(): [boolean, Observable<any>] {
    const isAccessTokenReady = this.tokenService.validateAccessToken();

    let response$: Observable<any>;

    if (isAccessTokenReady) {
      response$ = this.httpClient.get(this.USERS_SETTINGS_URL, this.apiRequestsService.getAuthorizationHeaders());
    } else {
      response$ = this.apiRequestsService.prepareGetNewAccessToken();
    }

    return [isAccessTokenReady, response$];
  }

  /**
   * Create the settings changed payload.
   *
   * @param language the language.
   * @param adultSetting the adult setting.
   * @param excluded_channel_list the excluded channel list.
   * @returns the payload.
   */
  private createSettingsChangePayload(language: string, adultSetting: boolean, excluded_channel_list: Set<number>): {} {
    let payload = {};

    if (language != null) {
      payload[this.SERVER_SETTING_LANGUAGE] = language;
    }

    if (adultSetting != null) {
      payload[this.SERVER_SETTING_ADULT] = adultSetting;
    }

    if (excluded_channel_list != null) {
      payload[this.SERVER_SETTING_EXCLUDED_CHANNEL_LIST] = Array.from(excluded_channel_list);
    }

    return payload;
  }

  /**
   * Prepare request for changing the settings.
   */
  public prepareSettingsChange(payload: {}): [boolean, Observable<any>] {
    const isAccessTokenReady = this.tokenService.validateAccessToken();

    let response$: Observable<any>;

    if (isAccessTokenReady) {
      response$ = this.httpClient.put(this.USERS_SETTINGS_URL, payload,
        this.apiRequestsService.getAuthorizationHeaders());
    } else {
      response$ = this.apiRequestsService.prepareGetNewAccessToken();
    }

    return [isAccessTokenReady, response$];
  }
}
