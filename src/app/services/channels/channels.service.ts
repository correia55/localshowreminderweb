import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { timeout } from 'rxjs/operators';
import { REQUEST_TIMEOUT, SERVER_URL } from 'src/app/constants';
import { Channel } from 'src/app/structs/Channel';
import { Utilities } from 'src/app/utilities';
import { TokenService } from '../token/token.service';

@Injectable({
  providedIn: 'root'
})
export class ChannelsService {
  private readonly CHANNELS_URL = SERVER_URL + '/channels';
  private readonly CHANNEL_LIST_STORAGE_ENTRY = 'channelList';
  private readonly CHANNEL_LIST_EXPIRATION_STORAGE_ENTRY = 'channelListExpirationDate';

  public channelList: [Channel];

  public showLoading$: BehaviorSubject<boolean>;

  constructor(private httpClient: HttpClient, tokenService: TokenService) {
    this.showLoading$ = new BehaviorSubject<boolean>(false);

    // This obtains the current list of channels, if the user is logged in
    if (tokenService.validateRefreshToken()) {
      this.getChannelList();
    }
  }

  /**
   * Get the complete list of channels.
   */
  public getChannelList() {
    if (Utilities.validateLocalData(localStorage.getItem(this.CHANNEL_LIST_EXPIRATION_STORAGE_ENTRY))) {
      this.channelList = JSON.parse(localStorage.getItem(this.CHANNEL_LIST_STORAGE_ENTRY));
    } else {
      this.getChannelListRequest();
    }
  }

  /**
   * Get the complete list of channels.
   */
  private getChannelListRequest() {
    this.showLoading$.next(true);

    this.prepareGetChannelList().pipe(timeout(REQUEST_TIMEOUT)).subscribe(
      response => { this.saveChannelList(response); },
      error => { },
      () => { this.showLoading$.next(false); }
    );
  }

  /**
   * Prepare request for obtaining the complete list of channels.
   *
   * @param verificationToken the verification token.
   */
  public prepareGetChannelList(): Observable<any> {
    return this.httpClient.get(this.CHANNELS_URL);
  }

  /**
   * Save the channel list in the local storage.
   *
   * @param channelList the channel list.
   */
  public saveChannelList(channelList) {
    const expirationDate = new Date();
    expirationDate.setDate(expirationDate.getDate() + 7);

    localStorage.setItem(this.CHANNEL_LIST_STORAGE_ENTRY, JSON.stringify(channelList));
    localStorage.setItem(this.CHANNEL_LIST_EXPIRATION_STORAGE_ENTRY, expirationDate.toString());

    this.channelList = channelList;
  }
}
