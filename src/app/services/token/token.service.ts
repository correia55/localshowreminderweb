import { Injectable } from '@angular/core';
import { Utilities } from 'src/app/utilities';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  public static readonly RESPONSE_TOKEN = 'token';

  /**
   * Get the user's language of preference.
   */
  public getUsername() {
    return localStorage.getItem('username');
  }

  /**
   * Checks if the refresh token is still valid.
   */
  public validateRefreshToken(): boolean {
    const validToken = Utilities.validateLocalData(localStorage.getItem('refreshTokenExpirationDate'));

    if (!validToken) {
      this.deleteAccessToken();
      this.deleteRefreshToken();
    }

    return validToken;
  }

  /**
   * Checks if the access token is still valid.
   */
  public validateAccessToken(): boolean {
    const validToken = Utilities.validateLocalData(sessionStorage.getItem('accessTokenExpirationDate'));

    if (!validToken) {
      this.deleteAccessToken();
    }

    return validToken;
  }

  /**
   * Save the refresh token in the local storage.
   *
   * @param refreshToken the refresh token.
   */
  public saveRefreshToken(refreshToken: string, username: string) {
    const expirationDate = new Date();
    expirationDate.setDate(expirationDate.getDate() + 364);

    localStorage.setItem('refreshToken', refreshToken);
    localStorage.setItem('refreshTokenExpirationDate', expirationDate.toString());
    localStorage.setItem('username', username)
  }

  /**
   * Save the access token in the session storage.
   *
   * @param accessToken the access token.
   */
  public saveAccessToken(accessToken: string) {
    const expirationDate = new Date();
    expirationDate.setHours(expirationDate.getHours() + 1);

    sessionStorage.setItem('accessToken', accessToken);
    sessionStorage.setItem('accessTokenExpirationDate', expirationDate.toString());
  }

  /**
   * Get the refresh token.
   */
  public getRefreshToken() {
    if (!this.validateRefreshToken()) {
      throw new Error('invalidRefreshToken');
    }

    return localStorage.getItem('refreshToken');
  }

  /**
   * Get the access token.
   */
  public getAccessToken() {
    if (!this.validateAccessToken()) {
      throw new Error('invalidAccessToken');
    }

    return sessionStorage.getItem('accessToken');
  }

  /**
   * Delete the refresh token from storage.
   */
  public deleteRefreshToken() {
    localStorage.removeItem('refreshToken');
    localStorage.removeItem('refreshTokenExpirationDate');
    localStorage.removeItem('username');
  }

  /**
   * Delete the access token from storage.
   */
  public deleteAccessToken() {
    sessionStorage.removeItem('accessToken');
    sessionStorage.removeItem('accessTokenExpirationDate');
  }
}
