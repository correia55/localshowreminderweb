import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { OverlayContainer } from '@angular/cdk/overlay';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  LIGHT_THEME = 'light-theme';
  DARK_THEME = 'dark-theme';

  themeClass$: BehaviorSubject<string>;
  logoSrc$: BehaviorSubject<string>;

  constructor(private overlay: OverlayContainer) {
    const savedThemClass = localStorage.getItem('theme');

    if (savedThemClass == null) {
      this.themeClass$ = new BehaviorSubject<string>(this.LIGHT_THEME);
    } else {
      this.themeClass$ = new BehaviorSubject<string>(savedThemClass);
    }

    this.logoSrc$ = new BehaviorSubject<string>('');

    this.setBodyContainerTheme(this.themeClass$.getValue());
    this.setOverlayContainerTheme(this.themeClass$.getValue());
    this.changeLogoBasedOnTheme();
  }

  /**
   * Change the theme of the application.
   */
  public changeTheme(): void {
    const previousTheme = this.themeClass$.getValue();

    if (this.themeClass$.getValue() === this.LIGHT_THEME) {
      this.themeClass$.next(this.DARK_THEME);
    } else {
      this.themeClass$.next(this.LIGHT_THEME);
    }

    this.setBodyContainerTheme(this.themeClass$.getValue(), previousTheme);
    this.setOverlayContainerTheme(this.themeClass$.getValue(), previousTheme);
    this.changeLogoBasedOnTheme();

    // Save the current theme in the cookies
    localStorage.setItem('theme', this.themeClass$.getValue());
  }

  /**
   * Change the theme of the overlay container.
   */
  setOverlayContainerTheme(newTheme: string, oldTheme?: string) {
    if (oldTheme) {
      this.overlay.getContainerElement().classList.remove(oldTheme);
    }

    this.overlay.getContainerElement().classList.add(newTheme);
  }

  /**
   * Change the theme of the body.
   */
  setBodyContainerTheme(newTheme: string, oldTheme?: string) {
    if (oldTheme) {
      document.body.classList.remove(oldTheme);
    }

    document.body.classList.add(newTheme);
  }

  /**
   * Change the logo based on the current theme.
   */
  changeLogoBasedOnTheme() {
    if (this.themeClass$.getValue() === this.LIGHT_THEME) {
      this.logoSrc$.next('assets/logo.png');
    } else {
      this.logoSrc$.next('assets/darkLogo.png');
    }
  }
}
