import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { timeout } from 'rxjs/operators';
import { BooleanWrapper } from 'src/app/booleanWrapper';
import { CheckEmailSnackBarComponent } from 'src/app/components/snack-bar/check-email-snackbar-component';
import { CredentialsSnackBarComponent } from 'src/app/components/snack-bar/credentials-snackbar-component';
import { GenericProblemSnackBarComponent } from 'src/app/components/snack-bar/generic-problem-snackbar-component';
import { REQUEST_TIMEOUT, SERVER_URL, UNAUTHORIZED_ACCESS } from 'src/app/constants';
import { ManagerService } from '../manager/manager.service';
import { TokenService } from '../token/token.service';

@Injectable({
  providedIn: 'root'
})
export class SendVerificationEmailService {
  private readonly SEND_VERIFICATION_EMAIL_URL = SERVER_URL + '/send-verification-email';

  constructor(private manager: ManagerService,
              public tokenService: TokenService,
              private httpClient: HttpClient) { }

  /**
   * Send a new verification email.
   * @param email the email of the user.
   * @param snackBar the snackbar of the caller component.
   * @param showLoading the show loading for some component.
   */
  public sendNewVerificationEmail(email: string, snackBar: MatSnackBar, showLoading: BooleanWrapper) {
    this.manager.startProcessing(showLoading);

    const response$ = this.httpClient.post(this.SEND_VERIFICATION_EMAIL_URL, { email }, {});

    response$.pipe(timeout(REQUEST_TIMEOUT)).subscribe(
      response => {
        snackBar.openFromComponent(CheckEmailSnackBarComponent, {
          duration: 5000,
        });

        this.manager.stopProcessing(showLoading);
      },
      error => {
        if (error.error === UNAUTHORIZED_ACCESS) {
          snackBar.openFromComponent(CredentialsSnackBarComponent, {
            duration: 5000,
          });
        } else {
          snackBar.openFromComponent(GenericProblemSnackBarComponent, {
            duration: 5000,
          });
        }

        this.manager.stopProcessing(showLoading);
      }
    );
  }
}
