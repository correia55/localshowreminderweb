import { TestBed } from '@angular/core/testing';

import { SendVerificationEmailService } from './send-verification-email.service';

describe('SendVerificationEmailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SendVerificationEmailService = TestBed.get(SendVerificationEmailService);
    expect(service).toBeTruthy();
  });
});
