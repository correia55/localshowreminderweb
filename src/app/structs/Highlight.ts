import { DbShowData } from './DbShowData';

export class Highlight {
  key: string;
  year: number;
  week: number;
  show_list: DbShowData[];
}
