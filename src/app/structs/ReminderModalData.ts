import { Reminder } from './Reminder';
import { LocalShowData } from './LocalShowData';

export class ReminderModalData {
  sessionId: number;
  title: string;
  season: number;
  episode: number;
  dateTime: Date;

  channelName: string;

  reminderId: number;
  reminderAnticipationHours: number;

  static createFromReminder(showReminder: Reminder): ReminderModalData {
    const reminderModalData = new ReminderModalData();

    reminderModalData.sessionId = showReminder.session_id;
    reminderModalData.title = showReminder.title;
    reminderModalData.dateTime = showReminder.date_time;

    if (showReminder.season) {
      reminderModalData.season = showReminder.season;
    }

    if (showReminder.episode) {
      reminderModalData.episode = showReminder.episode;
    }

    reminderModalData.channelName = showReminder.channel_name;

    reminderModalData.reminderId = showReminder.id;
    reminderModalData.reminderAnticipationHours = showReminder.anticipation_minutes / 60;

    return reminderModalData;
  }

  static createFromLocalShowData(show: LocalShowData): ReminderModalData {
    const reminderModalData = new ReminderModalData();

    reminderModalData.sessionId = show.id;
    reminderModalData.title = show.title;
    reminderModalData.season = show.season;
    reminderModalData.episode = show.episode;
    reminderModalData.dateTime = show.date_time;

    reminderModalData.channelName = show.service;

    if (show.reminder != null) {
      reminderModalData.reminderId = show.reminder.id;
      reminderModalData.reminderAnticipationHours = show.reminder.anticipation_minutes / 60;
      reminderModalData.sessionId = show.reminder.session_id;
    }

    return reminderModalData;
  }
}
