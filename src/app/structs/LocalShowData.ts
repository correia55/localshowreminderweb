import { Reminder } from './Reminder';

export class LocalShowData {
  // Technical
  id: number;
  type: string;

  // Common to both types
  title: string;
  service: string;  // Channel name or Streaming Service name
  is_movie: boolean; // Optional
  year: number; // Optional
  extended_cut: boolean; // Optional

  // TV
  season: number; // Optional
  episode: number; // Optional
  date_time: Date; // Optional
  audio_language: string; // Optional

  // Streaming
  first_season_available: number; // Optional
  last_season_available: number; // Optional

  // Reminder - when this show is a match for a reminder
  reminder: Reminder;
  isMatch: boolean;
}
