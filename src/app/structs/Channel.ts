export class Channel {
  id: number;
  name: string;
  adult: boolean;
}
