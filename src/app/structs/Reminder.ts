export class Reminder {
  id: number;
  anticipation_minutes: number;
  session_id: number;

  title: string;
  season: number;
  episode: number;
  date_time: Date;

  channel_name: string;
}
