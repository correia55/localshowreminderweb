import { Alarm } from './Alarm';

export class DbShowData {
  is_movie: boolean;
  show_image: string;
  trakt_id: string;
  show_title: string;
  translated_title: string;
  show_year: number;
  show_overview: string;
  vote_average: number;
  vote_count: number;
  season_premiere: number;
  language: string;

  isMatch: boolean;
  alarm: Alarm;
}
