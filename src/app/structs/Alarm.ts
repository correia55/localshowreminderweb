export class Alarm {
  id: number;
  show_name: string;
  is_movie: boolean;
  alarm_type: string;
  show_episode: number;
  show_season: number;
  show_titles: [string];
  show_id: string;
}
