import { Alarm } from './Alarm';
import { DbShowData } from './DbShowData';

export enum DataType {
  DB = 'DB',
  LISTINGS = 'LISTINGS'
}

export class AlarmModalData {
  showTitle: string;
  traktId: string;
  showIsMovie: boolean;
  showSeason: number;
  showEpisode: number;
  showLanguage: string;

  alarmId: number;
  alarmType: DataType;
  alarmSeason: number;
  alarmEpisode: number;

  static createFromAlarm(showAlarm: Alarm): AlarmModalData {
    const alarmModalData = new AlarmModalData();

    alarmModalData.showTitle = showAlarm.show_name;
    alarmModalData.showIsMovie = showAlarm.is_movie;

    if (!alarmModalData.showIsMovie) {
      alarmModalData.showSeason = 1;
      alarmModalData.showEpisode = 1;
    }

    alarmModalData.alarmId = showAlarm.id;
    alarmModalData.alarmType = DataType[showAlarm.alarm_type];
    alarmModalData.alarmSeason = showAlarm.show_season;
    alarmModalData.alarmEpisode = showAlarm.show_episode;

    return alarmModalData;
  }

  static createFromDbShowData(show: DbShowData): AlarmModalData {
    const alarmModalData = new AlarmModalData();

    alarmModalData.showTitle = show.show_title;
    alarmModalData.showIsMovie = show.is_movie;
    alarmModalData.traktId = show.trakt_id;
    alarmModalData.showLanguage = show.language;

    if (!alarmModalData.showIsMovie) {
      alarmModalData.showSeason = 1;
      alarmModalData.showEpisode = 1;
    }

    alarmModalData.alarmType = DataType.DB;

    if (show.alarm != null) {
      alarmModalData.alarmId = show.alarm.id;
      alarmModalData.alarmSeason = show.alarm.show_season;
      alarmModalData.alarmEpisode = show.alarm.show_episode;
    }

    return alarmModalData;
  }
}
