import { Component, OnInit } from '@angular/core';
import { ApiRequestsService } from 'src/app/services/api-requests/api-requests.service';
import { TokenService } from 'src/app/services/token/token.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { passwordPolicyValidator, controlsEqualsValidator, controlsDifferentValidator } from '../shared/form-validators.directive';
import { SomethingWrongSnackBarComponent } from '../snack-bar/something-wrong-snackbar-component';
import { PasswordChangedSnackBarComponent } from '../snack-bar/password-changed-snackbar-component';
import { ACCESS_TOKEN_ERROR } from 'src/app/constants';

@Component({
  selector: 'app-password-change',
  templateUrl: './password-change.component.html',
  styleUrls: ['./password-change.component.css']
})
export class PasswordChangeComponent implements OnInit {
  // True if a loading icon should be shown
  showLoading: boolean;

  // The icons in front of each password
  currentPasswordIcon = 'visibility_off';
  newPasswordIcon = 'visibility_off';
  newPasswordRepIcon = 'visibility_off';

  // The form
  passwordChangeForm: FormGroup;

  constructor(private apiRequestsService: ApiRequestsService,
    public tokenService: TokenService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.createFormGroup();
  }

  private createFormGroup() {
    this.passwordChangeForm = new FormGroup({
      'currentPassword': new FormControl('', {
        validators: [
          Validators.required,
          passwordPolicyValidator()
        ],
        updateOn: 'change'
      }),
      'newPassword': new FormControl('', {
        validators: [
          Validators.required,
          passwordPolicyValidator()
        ],
        updateOn: 'change'
      }),
      'newPasswordRepetition': new FormControl('', {
        validators: [
          Validators.required
        ],
        updateOn: 'change'
      })
    }, {
      validators: [controlsDifferentValidator('currentPassword', 'newPassword'),
      controlsEqualsValidator('newPassword', 'newPasswordRepetition')],
      updateOn: 'change'
    });
  }

  get currentPassword(): AbstractControl { return this.passwordChangeForm.get('currentPassword'); }

  get newPassword(): AbstractControl { return this.passwordChangeForm.get('newPassword'); }

  get newPasswordRepetition(): AbstractControl { return this.passwordChangeForm.get('newPasswordRepetition'); }

  get currentPasswordValue(): string { return this.currentPassword.value.trim(); }

  get newPasswordValue(): string { return this.newPassword.value.trim(); }

  get newPasswordRepetitionValue(): string { return this.newPasswordRepetition.value.trim(); }

  /**
   * When the user submits the form.
   */
  public formSubmitted(callRepetition: number) {
    if (callRepetition > 1) {
      console.log('There was an error while changing the password!');

      this.showLoading = false;
      return;
    }

    if (callRepetition === 0) {
      // If the new password and it's repetition do not match
      if (this.newPasswordValue !== this.newPasswordRepetitionValue) {
        return;
      }

      this.showLoading = true;
    }

    // Prepare the change email request
    const prepareResponse = this.apiRequestsService.preparePasswordChange(this.currentPasswordValue, this.newPasswordValue);

    this.showLoading = true;

    if (prepareResponse[0]) {
      prepareResponse[1].subscribe(
        response => {
          this.snackBar.openFromComponent(PasswordChangedSnackBarComponent, {
            duration: 5000,
          });

          this.showLoading = false;
        },
        error => {
          this.snackBar.openFromComponent(SomethingWrongSnackBarComponent, {
            duration: 5000,
          });

          this.showLoading = false;
        });
    } else {
      prepareResponse[1].subscribe(
        response => {
          this.tokenService.saveAccessToken(response.token);
          this.formSubmitted(callRepetition + 1);
        },
        // Refresh token is no longer valid
        error => {
          if (error.error !== ACCESS_TOKEN_ERROR) {
            this.tokenService.deleteRefreshToken();
          }

          this.snackBar.openFromComponent(SomethingWrongSnackBarComponent, {
            duration: 5000,
          });

          this.showLoading = false;
        }
      );
    }
  }

  /**
   * Toggle between showing and hiding the current password.
   */
  toggleShowCurrentPassword(passwordFieldId: string) {
    const isVisible = this.currentPasswordIcon !== 'visibility';

    this.currentPasswordIcon = isVisible ? 'visibility' : 'visibility_off';
    this.changePasswordInputType(isVisible, passwordFieldId);
  }

  /**
   * Toggle between showing and hiding the new password.
   */
  toggleShowNewPassword(passwordFieldId: string) {
    const isVisible = this.newPasswordIcon !== 'visibility';

    this.newPasswordIcon = isVisible ? 'visibility' : 'visibility_off';
    this.changePasswordInputType(isVisible, passwordFieldId);
  }

  /**
   * Toggle between showing and hiding the new password repetition.
   */
  toggleShowNewPasswordRepetition(passwordFieldId: string) {
    const isVisible = this.newPasswordRepIcon !== 'visibility';

    this.newPasswordRepIcon = isVisible ? 'visibility' : 'visibility_off';
    this.changePasswordInputType(isVisible, passwordFieldId);
  }

  changePasswordInputType(isVisible: boolean, passwordFieldId: string) {
    const passwordField = document.getElementById(passwordFieldId);
    passwordField.setAttribute('type', isVisible ? 'text' : 'password');
  }
}
