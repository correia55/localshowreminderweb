import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTabGroup } from '@angular/material/tabs';
import { AlarmService } from 'src/app/services/alarm/alarm.service';
import { HighlightsService } from 'src/app/services/highlights/highlights.service';
import { Alarm } from 'src/app/structs/Alarm';
import { DbShowData } from 'src/app/structs/DbShowData';
import { Highlight } from 'src/app/structs/Highlight';
import { Utilities } from 'src/app/utilities';
import { ShowModalComponent } from '../show-modal/show-modal.component';

@Component({
  selector: 'app-highlights',
  templateUrl: './highlights.component.html',
  styleUrls: ['./highlights.component.css']
})
export class HighlightsComponent implements AfterViewInit {
  public scoreContainerClass: string;
  public newContainerClass: string;

  itemsPerRow: number;
  isSmallMobileDevice: boolean;

  @ViewChild('scoreTabGroup') scoreTabGroup: MatTabGroup;
  @ViewChild('newTabGroup') newTabGroup: MatTabGroup;

  constructor(private dialog: MatDialog, private alarmService: AlarmService,
    public highlightsService: HighlightsService) {
    this.isSmallMobileDevice = window.matchMedia('(max-width: 1279px)').matches;

    // Calculate the number of items that fit a row of the screen
    this.itemsPerRow = Math.min(Math.floor(window.screen.width / 180), 9);

    // Get the highlights
    this.highlightsService.scoreHighlights.subscribe(scoreHighlights => this.updateScoreHighlightsContainerClass(scoreHighlights));
    this.highlightsService.newHighlights.subscribe(newHighlights => this.updateNewHighlightsContainerClass(newHighlights));

    this.highlightsService.getHighlights([true]);

    // Get the alarms
    this.alarmService.alarms.subscribe(alarms => this.updateResultsWithAlarms(alarms));
  }

  ngAfterViewInit() {
    this.scoreTabGroup.selectedIndex = 1;

    if (this.highlightsService.newHighlights.getValue()[1] != null) {
      this.newTabGroup.selectedIndex = 1;
    }
  }

  public getScoreHighlights(): Highlight[] {
    return this.highlightsService.scoreHighlights.getValue();
  }

  public getNewHighlights(): Highlight[] {
    return this.highlightsService.newHighlights.getValue();
  }

  private updateScoreHighlightsContainerClass(scoreHighlights: Highlight[]) {
    if (scoreHighlights == null) {
      return;
    }

    if (scoreHighlights.length < this.itemsPerRow) {
      this.scoreContainerClass = "centeredContainer";
    } else {
      this.scoreContainerClass = "spacedBetweenContainer";
    }
  }

  private updateNewHighlightsContainerClass(newHighlights: Highlight[]) {
    if (newHighlights == null) {
      return;
    }

    if (newHighlights.length < this.itemsPerRow) {
      this.newContainerClass = "centeredContainer";
    } else {
      this.newContainerClass = "spacedBetweenContainer";
    }
  }

  /**
   * Update the results of the search with the match type, given the list of
   * alarms.
   *
   * @param alarms the list of alarms.
   */
  public updateResultsWithAlarms(alarms: Alarm[]): void {
    for (const highlight of this.highlightsService.scoreHighlights.getValue().concat(this.highlightsService.newHighlights.getValue())) {
      if (highlight == null) {
        continue;
      }

      for (const show of highlight.show_list) {
        Utilities.updateShowWithAlarms(show, alarms);
      }
    }
  }

  /**
   * Open a dialog to show the details.
   *
   * @param show the current show.
   */
  public onShowDetails(show: DbShowData) {
    this.dialog.open(ShowModalComponent, {
      data: [show, null], width: '900px'
    });
  }

  /**
   * Whether it should break row in the highlights.
   *
   * @param index the current index.
   * @returns whether it should break row in the highlights.
   */
  public shouldBreakRow(shows: DbShowData[], index: number): boolean {
    if (this.isSmallMobileDevice) {
      return false;
    }

    if(index == 0){
      return false;
    }

    return shows[index - 1].is_movie && !shows[index].is_movie;
  }
}
