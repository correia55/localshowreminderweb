import { Component, OnInit, Input, Inject } from '@angular/core';
import { AlarmModalData } from 'src/app/structs/AlarmModalData';
import { AlarmService } from 'src/app/services/alarm/alarm.service';
import { Validators, FormGroup, FormControl, AbstractControl } from '@angular/forms';
import { TokenService } from 'src/app/services/token/token.service';
import { AlarmFailedSnackBarComponent } from '../snack-bar/alarm-failed-snackbar-component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-create-alarm-modal',
  templateUrl: './create-alarm-modal.component.html',
  styleUrls: ['./create-alarm-modal.component.css']
})
export class CreateAlarmModalComponent implements OnInit {
  // The form
  createAlarmForm: FormGroup;

  @Input() public data: AlarmModalData;

  constructor(@Inject(MAT_DIALOG_DATA) public modalData: AlarmModalData,
              private dialogRef: MatDialogRef<CreateAlarmModalComponent>,
              private alarmService: AlarmService,
              private snackBar: MatSnackBar,
              public tokenService: TokenService) {
              this.data = modalData;
  }

  ngOnInit() {
    this.createFormGroup();
  }

  private createFormGroup() {
    this.createAlarmForm = new FormGroup({
      'season': new FormControl(this.data.showSeason, {
        validators: [
          Validators.required,
          Validators.pattern('[0-9]{1,4}'),
          Validators.max(50),
          Validators.min(1)
        ],
        updateOn: 'change'
      }),
      'episode': new FormControl(this.data.showEpisode, {
        validators: [
          Validators.required,
          Validators.pattern('[0-9]{1,4}'),
          Validators.min(1)
        ],
        updateOn: 'change'
      })
    });
  }

  get season(): AbstractControl { return this.createAlarmForm.get('season'); }

  get episode(): AbstractControl { return this.createAlarmForm.get('episode'); }

  get seasonValue(): number {
    // No need for trim, given that this is a number
    return this.season.value;
  }

  get episodeValue(): number {
    // No need for trim, given that this is a number
    return this.episode.value;
  }

  onCreateAlarm() {
    // Do not make the call if not logged in
    if (!this.tokenService.validateRefreshToken()) {
      this.snackBar.openFromComponent(AlarmFailedSnackBarComponent, {
        duration: 5000
      });

      return;
    }

    this.data.alarmSeason = this.seasonValue;
    this.data.alarmEpisode = this.episodeValue;

    this.alarmService.createAlarm(this.data, this.snackBar, 0);

    this.dialogRef.close();
  }
}
