import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

import { ApiRequestsService } from 'src/app/services/api-requests/api-requests.service';
import { AlarmService } from 'src/app/services/alarm/alarm.service';
import { DbShowData } from 'src/app/structs/DbShowData';
import { ACCESS_TOKEN_ERROR, SEARCH_SHOW_DB_URL } from 'src/app/constants';
import { MatPaginator } from '@angular/material/paginator';
import { Alarm } from 'src/app/structs/Alarm';
import { Utilities } from 'src/app/utilities';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { TokenService } from 'src/app/services/token/token.service';

@Component({
  selector: 'app-search-show-db',
  templateUrl: './search-show-db.component.html',
  styleUrls: ['./search-show-db.component.css']
})
export class SearchShowDBComponent implements OnInit {
  // The form
  searchForm: FormGroup;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  // References to the templates with the translated labels of the paginator
  @ViewChild('itemsPerPage') itemsPerPage: ElementRef;
  @ViewChild('previousPage') previousPage: ElementRef;
  @ViewChild('nextPage') nextPage: ElementRef;
  @ViewChild('of') of: ElementRef;

  public searchResults: DbShowData[];
  public pageResults: DbShowData[];
  public showLoading: boolean;
  public searchPerformed = false;
  public incompleteList = false;
  public pageSize: number;
  public containerClass: string;

  isSmallMobileDevice: boolean;
  itemsPerRow: number;

  constructor(private route: ActivatedRoute,
    private router: Router, private dialog: MatDialog,
    private apiRequestsService: ApiRequestsService,
    private tokenService: TokenService, private alarmService: AlarmService) {
    this.searchResults = [];

    this.isSmallMobileDevice = window.matchMedia('(max-width: 1279px)').matches;

    // Calculate the number of rows that fit the screen
    var rows = Math.floor(window.screen.height / 2 / 280) + 1;

    // Calculate the number of items that fit a row of the screen
    this.itemsPerRow = Math.min(Math.floor(window.screen.width / 180), 9);

    this.pageSize = this.itemsPerRow * rows;

    this.alarmService.alarms.subscribe(alarms => this.updateResultsWithAlarms(alarms));
  }

  ngOnInit() {
    this.createFormGroup();
    this.route.paramMap.subscribe(params => {
      const searchTextQuery = params.get('searchText');

      if (!searchTextQuery) {
        return;
      }

      this.setSearchText = searchTextQuery;

      this.searchShows(0);
    });
  }

  /**
   * Make the request for a search from TMDB.
   * @param callRepetition the current call of this function.
   */
  private searchShows(callRepetition: number) {
    if (callRepetition > 1) {
      console.log('There was an error, we were unable to search shows!');
      return;
    }

    if (callRepetition === 0) {
      this.showLoading = true;
    }

    try {
      const prepareResponse = this.apiRequestsService.prepareSearchDB(this.searchTextValue);

      if (prepareResponse[0]) {
        prepareResponse[1].subscribe(
          response => {
            this.updateResults(response['show_list'], 'remark' in response)
          },
          error => {
            this.updateResults([], false)
          }
        );
      } else {
        prepareResponse[1].subscribe(
          response => {
            this.tokenService.saveAccessToken(response.token);
            this.searchShows(callRepetition + 1);
          },
          // Refresh token is no longer valid
          error => {
            if (error.error !== ACCESS_TOKEN_ERROR) {
              this.tokenService.deleteRefreshToken();
            }

            this.updateResults([], false)

            // Call the function again, since the token is optional in this request
            this.searchShows(callRepetition + 1);
          }
        );
      }
    } catch (error) {
      console.log(error);

      this.showLoading = false;
    }
  }

  /**
   * Do everything related with the update of the list of results.
   *
   * @param searchResults the list of results.
   */
  private updateResults(searchResults, remark: boolean) {
    this.searchResults = searchResults;

    // Whether there are more results or not
    this.incompleteList = remark;

    this.updateResultsWithAlarms(this.alarmService.alarms.getValue());

    // Add a fake entry for a direct search to the sessions
    this.searchResults.push(new DbShowData());

    this.setPaginatorLabels();
    this.getPageResults();
    this.paginator.firstPage();

    if (this.searchResults.length < this.itemsPerRow) {
      this.containerClass = "centeredContainer";
    } else {
      this.containerClass = "spacedBetweenContainer";
    }

    this.showLoading = false;
    this.searchPerformed = true;
  }

  private createFormGroup() {
    this.searchForm = new FormGroup({
      'searchText': new FormControl('', {
        validators: [
          Validators.required
        ],
        updateOn: 'change'
      })
    });
  }

  get searchText(): AbstractControl { return this.searchForm.get('searchText'); }

  get searchTextValue(): string { return this.searchText.value.trim(); }

  set setSearchText(searchText: string) { this.searchText.setValue(searchText); }

  /**
   * Update the results of the search with the match type, given the list of
   * alarms.
   *
   * @param alarms the list of alarms.
   */
  public updateResultsWithAlarms(alarms: Alarm[]): void {
    for (const show of this.searchResults) {
      Utilities.updateShowWithAlarms(show, alarms);
    }
  }

  /**
   * Set the paginator labels with the correct translations.
   * Source: https://stackoverflow.com/questions/47593692/how-to-translate-mat-paginator-in-angular-4#47594193
   * Answer from NatoBoram
   */
  setPaginatorLabels() {
    this.paginator._intl.itemsPerPageLabel = this.itemsPerPage.nativeElement.innerText;
    this.paginator._intl.previousPageLabel = this.previousPage.nativeElement.innerText;
    this.paginator._intl.nextPageLabel = this.nextPage.nativeElement.innerText;

    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number): string => {
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;

      return (startIndex + 1) + ' - ' + endIndex + ' ' + this.of.nativeElement.innerText + ' ' + length;
    };
  }

  /**
   * Set the paginator labels with the correct translations.
   * Source: https://stackoverflow.com/questions/47593692/how-to-translate-mat-paginator-in-angular-4#47594193
   * Answer from NatoBoram
   */
  getPageResults() {
    const length = this.searchResults.length;

    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    const endIndex = startIndex < length ? Math.min(startIndex + this.paginator.pageSize, length) : startIndex + this.paginator.pageSize;

    this.pageResults = this.searchResults.slice(startIndex, endIndex);
  }

  /**
   * When the user clicks to search Show DB.
   */
  public onSearchShowDB(): void {
    this.router.navigate([SEARCH_SHOW_DB_URL, this.searchTextValue]);
  }

  /**
   * Used when the table page is changed.
   * (Update the page results to the new page.)
   */
  public onPageChange() {
    this.getPageResults();

    Utilities.scrollToTheTop(this.isSmallMobileDevice);
  }
}
