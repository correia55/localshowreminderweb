import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ApiRequestsService } from 'src/app/services/api-requests/api-requests.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { LOGIN_URL } from 'src/app/constants';
import { CheckEmailSnackBarComponent } from '../snack-bar/check-email-snackbar-component';
import { SomethingWrongSnackBarComponent } from '../snack-bar/something-wrong-snackbar-component';

@Component({
  selector: 'app-password-recovery',
  templateUrl: './password-recovery.component.html',
  styleUrls: ['./password-recovery.component.css']
})
export class PasswordRecoveryComponent implements OnInit {
  // The form
  recoverForm: FormGroup;

  public showLoading: boolean;

  constructor(private route: ActivatedRoute, private router: Router,
    private apiRequestsService: ApiRequestsService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.createRecoverFormGroup();

    this.route.paramMap.subscribe(params => {
      this.recoverForm.setValue({ email: params.get('email') });
    });
  }

  private createRecoverFormGroup() {
    this.recoverForm = new FormGroup({
      'email': new FormControl('', {
        validators: [
          Validators.required,
          Validators.pattern('[^@]+@[^@]+.[^@.]+')
        ],
        updateOn: 'change'
      })
    });
  }

  get email(): AbstractControl { return this.recoverForm.get('email'); }

  get emailValue(): string { return this.email.value.trim(); }

  /**
   * When the submits the form.
   */
  onSubmit() {
    // Prepare the recovery request
    const prepareResponse = this.apiRequestsService.prepareSendPasswordRecoveryEmail(this.emailValue);

    this.showLoading = true;

    prepareResponse.subscribe(
      () => {
        this.showLoading = false;

        this.snackBar.openFromComponent(CheckEmailSnackBarComponent, {
          duration: 5000,
        });
      },
      () => {
        this.showLoading = false;

        this.snackBar.openFromComponent(SomethingWrongSnackBarComponent, {
          duration: 5000,
        });
      }
    );
  }

  /**
   * When the user clicks a link.
   *
   * @param key the key of the url to navigate to.
   */
  public onNavigate(key: string) {
    switch (key) {
      case 'login':
        this.router.navigate([LOGIN_URL]);
        break;
    }
  }
}
