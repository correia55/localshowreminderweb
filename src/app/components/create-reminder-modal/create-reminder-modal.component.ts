import { Component, OnInit, Input, Inject } from '@angular/core';
import { ReminderModalData } from 'src/app/structs/ReminderModalData';
import { Validators, FormGroup, FormControl, AbstractControl } from '@angular/forms';
import { TokenService } from 'src/app/services/token/token.service';
import { ReminderFailedSnackBarComponent } from '../snack-bar/reminder-failed-snackbar-component';
import { ReminderService } from 'src/app/services/reminder/reminder.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-create-reminder-modal',
  templateUrl: './create-reminder-modal.component.html',
  styleUrls: ['./create-reminder-modal.component.css']
})
export class CreateReminderModalComponent implements OnInit {
  // The form
  createReminderForm: FormGroup;

  @Input() public data: ReminderModalData;

  constructor(@Inject(MAT_DIALOG_DATA) public modalData: ReminderModalData,
    private dialogRef: MatDialogRef<CreateReminderModalComponent>,
    private reminderService: ReminderService,
    private snackBar: MatSnackBar,
    public tokenService: TokenService) {
    this.data = modalData;
  }

  ngOnInit() {
    this.createFormGroup();
  }

  private createFormGroup() {
    this.createReminderForm = new FormGroup({
      'anticipationHours': new FormControl(1, {
        validators: [
          Validators.required,
          Validators.pattern('[0-9]{1,2}'),
          Validators.max(24),
          Validators.min(1)
        ],
        updateOn: 'change'
      })
    });
  }

  get anticipationHours(): AbstractControl { return this.createReminderForm.get('anticipationHours'); }

  get anticipationHoursValue(): number {
    // No need for trim, given that this is a number
    return this.anticipationHours.value;
  }

  onCreateReminder() {
    // Do not make the call if not logged in
    if (!this.tokenService.validateRefreshToken()) {
      this.snackBar.openFromComponent(ReminderFailedSnackBarComponent, {
        duration: 5000
      });

      return;
    }

    this.data.reminderAnticipationHours = this.anticipationHoursValue;

    this.reminderService.createReminder(this.data, this.snackBar, 0);

    this.dialogRef.close();
  }
}
