import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CreateReminderModalComponent } from './create-reminder-modal.component';

describe('CreateReminderModalComponent', () => {
  let component: CreateReminderModalComponent;
  let fixture: ComponentFixture<CreateReminderModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateReminderModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateReminderModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
