import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/services/token/token.service';
import { MAIN_URL, PASSWORD_CHANGE_URL, REQUEST_LOADING_THRESHOLD } from 'src/app/constants';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SettingsService } from 'src/app/services/settings/settings.service';
import { GenericProblemSnackBarComponent } from '../snack-bar/generic-problem-snackbar-component';
import { takeWhile, finalize } from 'rxjs/operators';
import { ChannelsService } from 'src/app/services/channels/channels.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  public language: string;
  public includeAdultsChannels: boolean;
  public excludedChannelSet: Set<number>;

  public saveSettingsVisible: boolean = false;

  public showLoading: boolean;

  public isMobileDevice: boolean;

  constructor(private router: Router, public tokenService: TokenService,
    public settingsService: SettingsService,
    public channelsService: ChannelsService,
    private snackBar: MatSnackBar) {
      this.isMobileDevice = window.matchMedia('(max-width: 1279px)').matches;
  }

  ngOnInit() {
    const isLoggedIn = this.tokenService.validateRefreshToken();

    // If not logged in, return to main page
    if (!isLoggedIn) {
      this.router.navigate([MAIN_URL]);
    }

    // Check if we already have the settings
    this.updateComponentSettings();

    if (!this.language) {
      // If there's no call being done, make a new one
      if (!this.settingsService.showLoading$.getValue()) {
        this.settingsService.getSettingsList();
      }

      // Subscribe to loading until it is no longer loading
      this.settingsService.showLoading$.pipe(takeWhile(loading => loading),
        // When finished loading
        finalize(() => {
          this.showLoading = false;
          this.getSettings();
        }))
        // Wait the threshold time before setting the loading screen
        // to prevent the screen from appearing when the load is fast
        .subscribe(() => setTimeout(() => {
          this.showLoading = this.settingsService.showLoading$.getValue();
        }, REQUEST_LOADING_THRESHOLD));
    }
  }

  /**
   * Get the settings from the service.
   * If we can't get the settings, navigate to the main page.
   */
  private getSettings() {
    this.updateComponentSettings();

    if (!this.language) {
      this.snackBar.openFromComponent(GenericProblemSnackBarComponent, {
        duration: 5000,
      });

      this.router.navigate([MAIN_URL]);
    }
  }

  /**
   * Update the component settings with the values from the service.
   */
  private updateComponentSettings() {
    this.includeAdultsChannels = this.settingsService.getUserAdultSetting();
    this.language = this.settingsService.getUserLanguage();
    this.excludedChannelSet = new Set(this.settingsService.getUserExcludedChannelListSetting());
  }

  /**
   * When the user clicks the change password button.
   */
  public changePassword() {
    this.router.navigate([PASSWORD_CHANGE_URL]);
  }

  /**
   * When the user clicks the change email button.
   */
  public changeEmail() {
    this.settingsService.changeEmail(this.snackBar);

    // Subscribe to loading until it is no longer loading
    this.settingsService.showLoading$.pipe(takeWhile(loading => loading),
      // When finished loading
      finalize(() => {
        this.showLoading = false;
      }))
      // Wait the threshold time before setting the loading screen
      // to prevent the screen from appearing when the load is fast
      .subscribe(() => setTimeout(() => {
        this.showLoading = this.settingsService.showLoading$.getValue();
      }, REQUEST_LOADING_THRESHOLD));
  }

  /**
   * When the user clicks the delete account button.
   */
  public deleteAccount() {
    this.settingsService.deleteAccount(this.snackBar);

    // Subscribe to loading until it is no longer loading
    this.settingsService.showLoading$.pipe(takeWhile(loading => loading),
      // When finished loading
      finalize(() => {
        this.showLoading = false;
      }))
      // Wait the threshold time before setting the loading screen
      // to prevent the screen from appearing when the load is fast
      .subscribe(() => setTimeout(() => {
        this.showLoading = this.settingsService.showLoading$.getValue();
      }, REQUEST_LOADING_THRESHOLD));
  }

  /**
   * When the user clicks the change settings.
   */
  public saveSettings() {
    this.settingsService.updateUserSettings(this.includeAdultsChannels, this.language, this.excludedChannelSet, this.snackBar);

    // Subscribe to loading until it is no longer loading
    this.settingsService.showLoading$.pipe(takeWhile(loading => loading),
      // When finished loading
      finalize(() => {
        this.showLoading = false;
        this.updateComponentSettings();
        this.updateSaveSettingsVisibility();
      }))
      // Wait the threshold time before setting the loading screen
      // to prevent the screen from appearing when the load is fast
      .subscribe(() => setTimeout(() => {
        this.showLoading = this.settingsService.showLoading$.getValue();
      }, REQUEST_LOADING_THRESHOLD));
  }

  /**
   * Check if a channel is excluded or not.
   * @param channelId the channel id.
   * @returns True if the channel is not excluded.
   */
  public isChannelExcluded(channelId: number): boolean {
    if (this.excludedChannelSet == null) {
      return true;
    }

    return !this.excludedChannelSet.has(channelId);
  }

  /**
   * Toggle a channel to either be excluded or not.
   * @param channelId the channel id.
   */
  public toggleChannelExcluded(channelId: number): void {
    if (this.excludedChannelSet == null) {
      return;
    }

    if (this.excludedChannelSet.has(channelId)) {
      this.excludedChannelSet.delete(channelId);
    } else {
      this.excludedChannelSet.add(channelId);
    }

    this.updateSaveSettingsVisibility();
  }

  /**
   * Toggle the adult setting.
   */
  public toggleAdultSetting() {
    this.includeAdultsChannels = !this.includeAdultsChannels;

    this.updateSaveSettingsVisibility();
  }

  /**
   * Language changed.
   */
  public languageChanged() {
    this.updateSaveSettingsVisibility();
  }

  /**
   * Update the save settings visibility.
   */
  private updateSaveSettingsVisibility(): void {
    this.saveSettingsVisible = this.settingsService.hasChanges(this.includeAdultsChannels, this.language, this.excludedChannelSet);
  }
}
