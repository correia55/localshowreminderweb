import { Component, OnInit } from '@angular/core';
import { ThemeService } from 'src/app/services/theme/theme.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  logoSrc = '';

  constructor(public themeService: ThemeService) { }

  ngOnInit() {
    this.themeService.logoSrc$.subscribe(logoSrc => this.logoSrc = logoSrc);
  }
}
