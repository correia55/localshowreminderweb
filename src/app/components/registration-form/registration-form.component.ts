import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { TokenService } from '../../services/token/token.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  LOGIN_URL, SIGNUP_URL, PASSWORD_RECOVERY_URL,
  MAIN_URL, EMAIL_CHANGE_URL, UNAUTHORIZED_ACCESS
} from 'src/app/constants';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { passwordPolicyValidator, controlsEqualsValidator } from '../shared/form-validators.directive';
import { SendVerificationEmailService } from 'src/app/services/send-verification-email/send-verification-email.service';
import { BooleanWrapper } from 'src/app/booleanWrapper';
import { UsersService } from 'src/app/services/users/users.service';

import { SocialAuthService } from "angularx-social-login";
import { GoogleLoginProvider } from "angularx-social-login";
import { Observable } from 'rxjs';
import { CredentialsSnackBarComponent } from '../snack-bar/credentials-snackbar-component';
import { SomethingWrongSnackBarComponent } from '../snack-bar/something-wrong-snackbar-component';
import { UnverifiedAccountSnackBarComponent } from '../snack-bar/unverified-account-snackbar-component';
import { AlarmService } from 'src/app/services/alarm/alarm.service';
import { SettingsService } from 'src/app/services/settings/settings.service';
import { ApiRequestsService } from 'src/app/services/api-requests/api-requests.service';
import { ChannelsService } from 'src/app/services/channels/channels.service';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})
export class RegistrationFormComponent implements OnInit {
  // The form
  public registrationForm: FormGroup;

  // The icons in front of each password
  public passwordIcon = 'visibility_off';
  public passwordRepIcon = 'visibility_off';

  // Used to know the phase of registration
  public hasRegistered = false;
  public emailParam: string;

  public loginUnverifiedAccount = false;

  // Whether to show a loading or not
  public showLoading: BooleanWrapper;

  constructor(private route: ActivatedRoute, private router: Router,
              private usersService: UsersService,
              public tokenService: TokenService, private snackBar: MatSnackBar,
              private emailService: SendVerificationEmailService,
              public settingsService: SettingsService,
              private alarmService: AlarmService,
              private channelsService: ChannelsService,
              private apiRequestsService: ApiRequestsService,
              private socialAuthService: SocialAuthService) {
                this.showLoading = new BooleanWrapper(false);
  }

  ngOnInit() {
    // This redirects the user when it already has logged in
    if (this.tokenService.validateRefreshToken()) {
      this.router.navigate([MAIN_URL]);
    }

    // Initialize the component according to whether or not it is the phase of
    // registration (and we don't have a parameter in the url) or after it
    this.route.paramMap.subscribe(params => {
      const registrationEmail = params.get('email');

      if (registrationEmail != null) {
        this.hasRegistered = true;
        this.emailParam = params.get('email');
      } else {
        this.createRegistrationFormGroup();
      }
    });
  }

  /**
   * Create the registration form.
   */
  private createRegistrationFormGroup() {
    if (this.registrationForm != null) {
      return;
    }

    this.registrationForm = new FormGroup(
      {
        'email': new FormControl('', {
          validators: [
            Validators.required,
            Validators.pattern('[^@]+@[^@]+.[^@.]+')
          ],
          updateOn: 'change'
        }),
        'password': new FormControl('', {
          validators: [
            Validators.required,
            passwordPolicyValidator()
          ],
          updateOn: 'change'
        }),
        'passwordRepetition': new FormControl('', {
          validators: [
            Validators.required
          ],
          updateOn: 'change'
        })
      },
      {
        validators: [controlsEqualsValidator('password', 'passwordRepetition')],
        updateOn: 'change'
      });
  }

  get email(): AbstractControl { return this.registrationForm.get('email'); }

  get password(): AbstractControl { return this.registrationForm.get('password'); }

  get passwordRepetition(): AbstractControl { return this.registrationForm.get('passwordRepetition'); }

  get emailValue(): string { return this.email.value.trim(); }

  get passwordValue(): string { return this.password.value.trim(); }

  get passwordRepetitionValue(): string { return this.passwordRepetition.value.trim(); }

  /**
   * When the user clicks a link.
   *
   * @param key the key of the url to navigate to.
   */
  public onNavigate(key: string) {
    switch (key) {
      case 'login':
        this.router.navigate([LOGIN_URL]);
        break;

      case 'forgotPassword':
        this.router.navigate([PASSWORD_RECOVERY_URL, this.emailValue]);
        break;
    }
  }

  /**
   * When the user toggles between showing and hiding the password repetition.
   */
  public onToggleShowPassword(passwordFieldId: string) {
    const isVisible = this.passwordIcon !== 'visibility';

    this.passwordIcon = isVisible ? 'visibility' : 'visibility_off';
    this.changePasswordInputType(isVisible, passwordFieldId);
  }

  /**
   * When the user toggles between showing and hiding the password repetition.
   */
  public onToggleShowPasswordRepetition(passwordFieldId: string) {
    const isVisible = this.passwordRepIcon !== 'visibility';

    this.passwordRepIcon = isVisible ? 'visibility' : 'visibility_off';
    this.changePasswordInputType(isVisible, passwordFieldId);
  }

  /**
   * Sign in using Google.
   */
  public signInWithGoogle(): void {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID)
      .then(response => {
        this.onExternalSignIn(response.idToken, 'GOOGLE');
      })
      .catch(err => console.log(err));
  }

  /**
   * When the user performs the sign in using an external source.
   */
  onExternalSignIn(token: string, source: string) {
    this.treatLoginObservable(this.apiRequestsService.prepareUserExternalLogin(token, source));
  }

  /**
   * Take care of any sign in, whether it is with email or from an external source.
   * @param signInObservable the sign in observable.
   */
  treatLoginObservable(signInObservable: Observable<any>) {
    // Show loading wheel
    this.showLoading.bool = true;

    signInObservable.subscribe(
      // When the request is a success
      response => {
        // Save the information from the response
        this.tokenService.saveRefreshToken(response.token, response.username);

        this.alarmService.getAlarmList();
        this.settingsService.getSettingsList();
        this.channelsService.getChannelList();

        // Navigate to the url where the user was before logging in
        this.router.navigate([MAIN_URL]);

        // Hide loading wheel
        this.showLoading.bool = false;
      },
      // When there's an error - open a snack bar with a message
      error => {
        if (error.error === UNAUTHORIZED_ACCESS) {
          this.snackBar.openFromComponent(CredentialsSnackBarComponent, {
            duration: 5000,
          });
        } else if (error.error === 'Unverified Account') {
          this.loginUnverifiedAccount = true;

          this.snackBar.openFromComponent(UnverifiedAccountSnackBarComponent, {
            duration: 5000,
          });
        } else {
          this.snackBar.openFromComponent(SomethingWrongSnackBarComponent, {
            duration: 5000,
          });
        }

        // Hide loading wheel
        this.showLoading.bool = false;
      }
    );
  }

  /**
   * When the user performs clicks the register button.
   */
  public onRegister() {
    this.usersService.prepareUserRegistration(this.emailValue, this.passwordValue,
      this.snackBar, this.showLoading,
      this.registrationWasSucces, this);
  }

  /**
   * Callback for when the registration is a success.
   * Can't use 'this' inside here, probably because it's a callback.
   */
  public registrationWasSucces(caller: RegistrationFormComponent) {
    caller.router.navigate([SIGNUP_URL, caller.emailValue]);
  }

  /**
   * When the user clicks to send a new verification email.
   */
  public onSendNewVerificationEmail() {
    this.emailService.sendNewVerificationEmail(this.emailParam, this.snackBar, this.showLoading);
  }

  /**
   * When the user clicks the fix email button.
   */
  public onFixEmail() {
    this.router.navigate([EMAIL_CHANGE_URL, this.emailParam]);
  }

  /**
   * Change the input type to text or password.
   * @param isVisible the content should be visible, thus type text.
   * @param passwordFieldId the id of the field.
   */
  private changePasswordInputType(isVisible: boolean, passwordFieldId: string) {
    const passwordField = document.getElementById(passwordFieldId);
    passwordField.setAttribute('type', isVisible ? 'text' : 'password');
  }
}
