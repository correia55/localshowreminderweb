import { Component, ElementRef, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { DbShowData } from '../../structs/DbShowData';
import { AlarmModalData } from 'src/app/structs/AlarmModalData';

@Component({
  selector: 'app-show-modal',
  templateUrl: './show-modal.component.html',
  styleUrls: ['./show-modal.component.css']
})
export class ShowModalComponent {

  public showAlarmModalData: AlarmModalData;

  constructor(@Inject(MAT_DIALOG_DATA) public data: [DbShowData, string]) {
    if (data[0]){
      this.showAlarmModalData = AlarmModalData.createFromDbShowData(data[0]);
    }
  }
}
