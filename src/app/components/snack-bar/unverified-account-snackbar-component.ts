import { Component } from '@angular/core';

@Component({
  template: '<span i18n="@@unverifiedAccount">Your account needs to be verified prior to logging in, check your email and follow the instructions!</span>',
})
export class UnverifiedAccountSnackBarComponent { }
