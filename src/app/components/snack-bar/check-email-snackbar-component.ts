import { Component } from '@angular/core';

@Component({
  template: '<span i18n="@@checkEmail">Check your email box!</span>',
})
export class CheckEmailSnackBarComponent { }
