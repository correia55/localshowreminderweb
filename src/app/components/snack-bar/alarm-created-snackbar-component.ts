import { Component } from '@angular/core';

@Component({
  template: '<span i18n="@@alarmCreated">Alarm created!</span>',
})
export class AlarmCreatedSnackBarComponent { }
