import { Component } from '@angular/core';

@Component({
  template: '<span i18n="@@somethingWrong">Something went wrong, try again later!</span>',
})
export class GenericProblemSnackBarComponent { }
