import { Component } from '@angular/core';

@Component({
  template: '<span i18n="@@alarmDeleted">Alarm deleted!</span>',
})
export class AlarmDeletedSnackBarComponent { }
