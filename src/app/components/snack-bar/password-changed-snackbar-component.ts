import { Component } from '@angular/core';

@Component({
  template: '<span i18n="@@passwordChanged">Password changed!</span>',
})
export class PasswordChangedSnackBarComponent { }
