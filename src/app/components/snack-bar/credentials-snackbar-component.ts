import { Component } from '@angular/core';

@Component({
  template: '<span i18n="@@invalidCredentials">Invalid credentials!</span>',
})
export class CredentialsSnackBarComponent { }
