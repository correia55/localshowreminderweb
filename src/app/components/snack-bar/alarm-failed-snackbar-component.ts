import { Component } from '@angular/core';

@Component({
  template: '<span i18n="@@alarmFailedLogin">Alarm failed, login needed!</span>',
})
export class AlarmFailedSnackBarComponent { }
