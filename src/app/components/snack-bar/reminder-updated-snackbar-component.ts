import { Component } from '@angular/core';

@Component({
  template: '<span i18n="@@reminderUpdated">Reminder updated!</span>',
})
export class ReminderUpdatedSnackBarComponent { }
