import { Component } from '@angular/core';

@Component({
  template: '<span i18n="@@sessionExpired">Your session has expired, please login again!</span>',
})
export class SessionExpiredSnackBarComponent { }
