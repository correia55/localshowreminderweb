import { Component } from '@angular/core';

@Component({
  template: '<span i18n="@@reminderCreated">Reminder created!</span>',
})
export class ReminderCreatedSnackBarComponent { }
