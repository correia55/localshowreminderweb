import { Component } from '@angular/core';

@Component({
  template: '<span i18n="@@reminderDeleted">Reminder deleted!</span>',
})
export class ReminderDeletedSnackBarComponent { }
