import { Component } from '@angular/core';

@Component({
  template: '<span i18n="@@alarmUpdated">Alarm updated!</span>',
})
export class AlarmUpdatedSnackBarComponent { }
