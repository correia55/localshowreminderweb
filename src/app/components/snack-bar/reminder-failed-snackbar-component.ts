import { Component } from '@angular/core';

@Component({
  template: '<span i18n="@@reminderFailedLogin">Reminder failed, login needed!</span>',
})
export class ReminderFailedSnackBarComponent { }
