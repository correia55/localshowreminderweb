import { Component, OnInit, Input, Inject } from '@angular/core';
import { AlarmService } from 'src/app/services/alarm/alarm.service';
import { AlarmModalData } from 'src/app/structs/AlarmModalData';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { anyValueChanged } from '../shared/form-validators.directive';

@Component({
  selector: 'app-update-alarm-modal',
  templateUrl: './update-alarm-modal.component.html',
  styleUrls: ['./update-alarm-modal.component.css']
})
export class UpdateAlarmModalComponent implements OnInit {
  // The form
  updateAlarmForm: FormGroup;

  @Input() public data: AlarmModalData;

  constructor(@Inject(MAT_DIALOG_DATA) public modalData: AlarmModalData,
              private dialogRef: MatDialogRef<UpdateAlarmModalComponent>,
              private alarmService: AlarmService,
              private snackBar: MatSnackBar) {
              this.data = modalData;
  }

  ngOnInit() {
    this.createFormGroup();
  }

  private createFormGroup() {
    this.updateAlarmForm = new FormGroup({
      'season': new FormControl(this.data.alarmSeason, {
        validators: [
          Validators.required,
          Validators.pattern('[0-9]{1,4}'),
          Validators.max(50),
          Validators.min(1)
        ],
        updateOn: 'change'
      }),
      'episode': new FormControl(this.data.alarmEpisode, {
        validators: [
          Validators.required,
          Validators.pattern('[0-9]{1,4}'),
          Validators.min(1)
        ],
        updateOn: 'change'
      })
    }, {
      validators: [anyValueChanged(['season', 'episode'], [this.data.alarmSeason.toString(), this.data.alarmEpisode.toString()])],
      updateOn: 'change'
    });
  }

  get season(): AbstractControl { return this.updateAlarmForm.get('season'); }

  get episode(): AbstractControl { return this.updateAlarmForm.get('episode'); }

  get seasonValue(): number {
    // No need for trim, given that this is a number
    return this.season.value;
  }

  get episodeValue(): number {
    // No need for trim, given that this is a number
    return this.episode.value;
  }

  onUpdateAlarm() {
    if (!this.data.showIsMovie) {
      if (this.seasonValue == null) {
        console.log('Invalid data!');
        return;
      }
    }

    this.data.alarmSeason = this.seasonValue;
    this.data.alarmEpisode = this.episodeValue;

    this.alarmService.updateAlarm(this.data, this.snackBar, 0);

    this.dialogRef.close();
  }

  onDeleteAlarm() {
    this.alarmService.deleteAlarm(this.data.alarmId, this.snackBar, 0);

    this.dialogRef.close();
  }
}
