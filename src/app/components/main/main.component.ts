import { Component, ViewChild, TemplateRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SEARCH_LISTINGS_URL, SEARCH_SHOW_DB_URL } from 'src/app/constants';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ThemeService } from 'src/app/services/theme/theme.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  // The form
  searchForm: FormGroup;

  logoSrc = '';

  constructor(private router: Router, public themeService: ThemeService) { }

  ngOnInit(): void {
    this.themeService.logoSrc$.subscribe(logoSrc => this.logoSrc = logoSrc);

    this.createFormGroup();
  }

  private createFormGroup() {
    this.searchForm = new FormGroup({
      'searchText': new FormControl('', {
        validators: [
          Validators.required
        ],
        updateOn: 'change'
      })
    });
  }

  get searchText(): AbstractControl { return this.searchForm.get('searchText'); }

  get searchTextValue() { return this.searchText.value.trim(); }

  set setSearchText(searchText: string) { this.searchText.setValue(searchText); }

  public onSearch() {
    this.router.navigate([SEARCH_SHOW_DB_URL, this.searchTextValue]);
  }
}
