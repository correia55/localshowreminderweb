import { Component, OnInit, Inject, Input } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AlarmService } from 'src/app/services/alarm/alarm.service';
import { AlarmModalData } from 'src/app/structs/AlarmModalData';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { AlarmFailedSnackBarComponent } from '../snack-bar/alarm-failed-snackbar-component';
import { TokenService } from 'src/app/services/token/token.service';
import { anyValueChanged } from '../shared/form-validators.directive';

@Component({
  selector: 'app-alarm-section',
  templateUrl: './alarm-section.component.html',
  styleUrls: ['./alarm-section.component.css']
})
export class AlarmSectionComponent implements OnInit {
  // The form
  alarmForm: FormGroup;

  @Input() public data: AlarmModalData;

  constructor(@Inject(MAT_DIALOG_DATA) public modalData: AlarmModalData,
              private dialogRef: MatDialogRef<AlarmSectionComponent>,
              private alarmService: AlarmService,
              private snackBar: MatSnackBar,
              public tokenService: TokenService) {
              this.data = modalData;
  }

  ngOnInit() {
    this.createFormGroup();
  }

  private createFormGroup() {
    var alarmSeason = 1
    var alarmEpisode = 1

    if(this.data.alarmSeason !== null && this.data.alarmSeason !== undefined){
      alarmSeason = this.data.alarmSeason;
    }else{
      this.data.alarmSeason = null;
    }

    if(this.data.alarmEpisode !== null && this.data.alarmEpisode !== undefined){
      alarmEpisode = this.data.alarmEpisode;
    }else{
      this.data.alarmEpisode = null;
    }

    this.alarmForm = new FormGroup({
      'season': new FormControl(alarmSeason, {
        validators: [
          Validators.required,
          Validators.pattern('[0-9]{1,4}'),
          Validators.max(50),
          Validators.min(1)
        ],
        updateOn: 'change'
      }),
      'episode': new FormControl(alarmEpisode, {
        validators: [
          Validators.required,
          Validators.pattern('[0-9]{1,4}'),
          Validators.min(1)
        ],
        updateOn: 'change'
      })
    }, {
      validators: [anyValueChanged(['season', 'episode'], ['' + this.data.alarmSeason, '' + this.data.alarmEpisode])],
      updateOn: 'change'
    });
  }

  get season(): AbstractControl { return this.alarmForm.get('season'); }

  get episode(): AbstractControl { return this.alarmForm.get('episode'); }

  get seasonValue(): number {
    // No need for trim, given that this is a number
    return this.season.value;
  }

  get episodeValue(): number {
    // No need for trim, given that this is a number
    return this.episode.value;
  }

  onSubmitData() {
    if (this.data.alarmId == null) {
      this.onCreateAlarm();
    } else {
      this.onUpdateAlarm();
    }
  }

  onCreateAlarm() {
    // Do not make the call if not logged in
    if (!this.tokenService || !this.tokenService.validateRefreshToken()) {
      this.snackBar.openFromComponent(AlarmFailedSnackBarComponent, {
        duration: 5000
      });

      return;
    }

    if (!this.data.showIsMovie) {
      if (this.seasonValue == null || this.episodeValue == null) {
        console.log('Invalid data!');
        return;
      }
    }

    this.data.alarmSeason = this.seasonValue;
    this.data.alarmEpisode = this.episodeValue;

    this.alarmService.createAlarm(this.data, this.snackBar, 0);

    this.dialogRef.close();
  }

  onUpdateAlarm() {
    if (this.seasonValue == null || this.episodeValue == null) {
      console.log('Invalid data!');
      return;
    }

    this.data.alarmSeason = this.seasonValue;
    this.data.alarmEpisode = this.episodeValue;

    this.alarmService.updateAlarm(this.data, this.snackBar, 0);

    this.dialogRef.close();
  }

  onDeleteAlarm() {
    this.alarmService.deleteAlarm(this.data.alarmId, this.snackBar, 0);

    this.dialogRef.close();
  }
}
