import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { TokenService } from '../../services/token/token.service';
import {
  LOGIN_URL, SIGNUP_URL, REMINDER_URL, MAIN_URL, SETTINGS_URL, RESTRICTED_PAGES,
  ALARM_URL
} from 'src/app/constants';
import { ApiRequestsService } from 'src/app/services/api-requests/api-requests.service';
import { AlarmService } from 'src/app/services/alarm/alarm.service';
import { SettingsService } from 'src/app/services/settings/settings.service';
import { ThemeService } from 'src/app/services/theme/theme.service';

@Component({
  selector: 'app-user-corner',
  templateUrl: './user-corner.component.html',
  styleUrls: ['./user-corner.component.css']
})
export class UserCornerComponent {
  public width: number;

  constructor(private router: Router, public tokenService: TokenService,
              private apiRequestsService: ApiRequestsService,
              public settingsService: SettingsService,
              private alarmService: AlarmService,
              private themeService: ThemeService) { }

  /**
   * Get the previous url, from the url parameters.
   */
  private getPreviousUrl() {
    if (this.router.url.includes('login')) {
      const splitRes = this.router.url.split('%2F');
      return splitRes.length > 1 ? splitRes[1] : '';
    }

    return decodeURIComponent(this.router.url);
  }

  /**
   * Navigate to the login page.
   */
  public goToLogIn() {
    this.router.navigate([LOGIN_URL, this.getPreviousUrl()]);
  }

  /**
   * Navigate to the sign up page.
   */
  public goToSignUp() {
    this.router.navigate([SIGNUP_URL]);
  }

  /**
   * Navigate to the alarms page.
   */
  public goToAlarms() {
    this.router.navigate([ALARM_URL]);
  }

  /**
   * Navigate to the reminders page.
   */
  public goToReminders() {
    this.router.navigate([REMINDER_URL]);
  }

  /**
   * Navigate to the settings page.
   */
  public goToSettings() {
    this.router.navigate([SETTINGS_URL]);
  }

  /**
   * Make a logout request.
   */
  public logOut() {
    const prepareResponse = this.apiRequestsService.prepareUserLogout();

    prepareResponse.subscribe(() => {
      this.tokenService.deleteAccessToken();
      this.tokenService.deleteRefreshToken();

      this.alarmService.clearAlarms();

      // Redirect the user from restricted pages to the main page
      for (const page of RESTRICTED_PAGES) {
        if (this.router.url.includes(page)) {
          this.router.navigate([MAIN_URL]);
          break;
        }
      }
    });
  }

  /**
   * Store the width of the user button, so that the other buttons can match it.
   */
  public updateButtonsWidth() {
    this.width = document.getElementById('account').clientWidth;
  }

  /**
   * Change the theme of the application.
   */
  public changeTheme() {
    this.themeService.changeTheme();
  }
}
