import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UserCornerComponent } from './user-corner.component';

describe('UserCornerComponent', () => {
  let component: UserCornerComponent;
  let fixture: ComponentFixture<UserCornerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UserCornerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCornerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
