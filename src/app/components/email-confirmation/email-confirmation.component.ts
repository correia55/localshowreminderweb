import { Component, OnInit, DebugElement } from '@angular/core';
import { ApiRequestsService } from 'src/app/services/api-requests/api-requests.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MAIN_URL } from 'src/app/constants';
import { UsersService } from 'src/app/services/users/users.service';

@Component({
  selector: 'app-verify-account',
  templateUrl: './email-confirmation.component.html',
  styleUrls: ['./email-confirmation.component.css']
})
export class EmailConfirmationComponent implements OnInit {

  public operationSuccess: boolean;
  public showLoading: boolean;

  public mode: string;

  public ACCOUNT_VERIFICATION = 'verification';
  public ACCOUNT_DELETION = 'deletion';
  public ACCOUNT_CHANGE = 'change';

  constructor(private router: Router, private route: ActivatedRoute,
    private usersService: UsersService) { }

  ngOnInit() {

    this.route.paramMap.subscribe(params => {
      this.mode = params.get('mode');
      const token = params.get('token');

      let prepareResponse;

      if (this.mode === this.ACCOUNT_VERIFICATION) {
        // Prepare the account verification
        prepareResponse = this.usersService.prepareAccountVerification(token);
      } else if (this.mode === this.ACCOUNT_DELETION) {
        // Prepare the account deletion
        prepareResponse = this.usersService.prepareAccountDeletion(token);
      } else if (this.mode === this.ACCOUNT_CHANGE) {
        // Prepare the account deletion
        prepareResponse = this.usersService.prepareAccountChange(token);
      } else {
        return;
      }

      this.showLoading = true;

      prepareResponse.subscribe(
        response => {
          this.operationSuccess = true;

          if (this.mode === this.ACCOUNT_DELETION) {
            this.clearStorage();
          }

          // Redirect to the main page after 30s
          setTimeout(() => {
            this.router.navigate([MAIN_URL]);
          }, 30000);

          this.showLoading = false;
        },
        error => {
          this.operationSuccess = false;
          this.showLoading = false;
        }
      );
    });
  }

  /**
   * Clear storage of everything.
   */
  clearStorage() {
    localStorage.clear();
    sessionStorage.clear();
  }
}
