import { Component, Input, TemplateRef, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MAIN_URL } from 'src/app/constants';
import { TokenService } from 'src/app/services/token/token.service';
import { Router } from '@angular/router';
import { Reminder } from 'src/app/structs/Reminder';
import { ReminderModalData } from 'src/app/structs/ReminderModalData';
import { UpdateReminderModalComponent } from '../update-reminder-modal/update-reminder-modal.component';
import { ReminderService } from 'src/app/services/reminder/reminder.service';
import { Utilities } from 'src/app/utilities';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-reminder-list',
  templateUrl: './reminder-list.component.html',
  styleUrls: ['./reminder-list.component.css']
})
export class ReminderListComponent implements AfterViewInit {
  @Input() public searchDatabaseInfo: TemplateRef<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  // References to the templates with the translated labels of the paginator
  @ViewChild('itemsPerPage') itemsPerPage: ElementRef;
  @ViewChild('previousPage') previousPage: ElementRef;
  @ViewChild('nextPage') nextPage: ElementRef;
  @ViewChild('of') of: ElementRef;

  // Needed to be accessible by the html
  public remindersDataSource = new MatTableDataSource<Reminder>();
  public reminderDisplayedColumns: string[];

  pageSize: number;

  isSmallMobileDevice: boolean;

  constructor(private dialog: MatDialog, public tokenService: TokenService,
    private snackBar: MatSnackBar, public reminderService: ReminderService,
    private router: Router) {
    const isLoggedIn = this.tokenService.validateRefreshToken();

    // If not logged in, return to main page
    if (!isLoggedIn) {
      this.router.navigate([MAIN_URL]);
    }

    this.reminderService.reminders.subscribe(response => this.remindersDataSource.data = response);

    this.isSmallMobileDevice = window.matchMedia('(max-width: 1279px)').matches;

    if (this.isSmallMobileDevice) {
      this.reminderDisplayedColumns = ['wholeInfo', 'date', 'reminder'];
      this.pageSize = 8;
    } else {
      this.reminderDisplayedColumns = ['title', 'season', 'episode', 'channel',
      'date', 'anticipation', 'reminder'];

      this.pageSize = 20;
    }
  }

  /** The paginator is only obtained after view init, thus why this is here. */
  ngAfterViewInit() {
    this.remindersDataSource.paginator = this.paginator;
    this.setPaginatorLabels();
  }

  /**
   * When the user updates a alarm.
   *
   * @param show the corresponding show.
   */
  public onUpdateReminder(reminder: Reminder) {
    const data = ReminderModalData.createFromReminder(reminder);

    this.dialog.open(UpdateReminderModalComponent, {
      data, width: '500px'
    });
  }

  /**
   * When the user deletes a reminder.
   *
   * @param show the corresponding show.
   */
  public onDeleteReminder(reminder: Reminder) {
    const data = ReminderModalData.createFromReminder(reminder);

    this.reminderService.deleteReminder(data.reminderId, this.snackBar, 0);
  }

  /**
   * Set the paginator labels with the correct translations.
   * Source: https://stackoverflow.com/questions/47593692/how-to-translate-mat-paginator-in-angular-4#47594193
   * Answer from NatoBoram
   */
  setPaginatorLabels() {
    this.paginator._intl.itemsPerPageLabel = this.itemsPerPage.nativeElement.innerText;
    this.paginator._intl.previousPageLabel = this.previousPage.nativeElement.innerText;
    this.paginator._intl.nextPageLabel = this.nextPage.nativeElement.innerText;

    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number): string => {
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;

      return (startIndex + 1) + ' - ' + endIndex + ' ' + this.of.nativeElement.innerText + ' ' + length;
    };
  }

  /**
   * Used when the table page is changed.
   */
  onPageChange() {
    Utilities.scrollToTheTop(this.isSmallMobileDevice);
  }
}
