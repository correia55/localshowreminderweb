import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiRequestsService } from 'src/app/services/api-requests/api-requests.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LOGIN_URL, MAIN_URL } from 'src/app/constants';
import { passwordPolicyValidator, controlsEqualsValidator } from '../shared/form-validators.directive';
import { PasswordChangedSnackBarComponent } from '../snack-bar/password-changed-snackbar-component';
import { SomethingWrongSnackBarComponent } from '../snack-bar/something-wrong-snackbar-component';

@Component({
  selector: 'app-password-recovery-change',
  templateUrl: './password-recovery-change.component.html',
  styleUrls: ['./password-recovery-change.component.css']
})
export class PasswordRecoveryChangeComponent implements OnInit {
  // The form
  recoveryForm: FormGroup;

  // The icons in front of each password
  passwordIcon = 'visibility_off';
  passwordRepIcon = 'visibility_off';

  public token: string;

  public showLoading: boolean;

  constructor(private route: ActivatedRoute, private router: Router,
    private apiRequestsService: ApiRequestsService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.token = params.get('token');

      if (!this.token) {
        this.router.navigate([MAIN_URL]);
      }
    });

    this.createPasswordRecoveryChangeFormGroup();
  }

  private createPasswordRecoveryChangeFormGroup() {
    this.recoveryForm = new FormGroup({
      'password': new FormControl('', {
        validators: [
          Validators.required,
          passwordPolicyValidator()
        ],
        updateOn: 'change'
      }),
      'passwordRepetition': new FormControl('', {
        validators: [
          Validators.required
        ],
        updateOn: 'change'
      })
    }, {
      validators: [controlsEqualsValidator('password', 'passwordRepetition')],
      updateOn: 'change'
    });
  }

  get password(): AbstractControl { return this.recoveryForm.get('password'); }

  get passwordRepetition(): AbstractControl { return this.recoveryForm.get('passwordRepetition'); }

  get passwordValue(): string { return this.password.value.trim(); }

  get passwordRepetitionValue(): string { return this.passwordRepetition.value.trim(); }

  /**
   * When the user presses the button.
   */
  onSubmit() {
    // Prepare the login request
    const prepareResponse = this.apiRequestsService.preparePasswordRecovery(this.token, this.passwordValue);

    this.showLoading = true;

    prepareResponse.subscribe(
      () => {
        this.snackBar.openFromComponent(PasswordChangedSnackBarComponent, {
          duration: 5000,
        });

        this.showLoading = false;

        // Redirect to the main page after 30s
        setTimeout(() => {
          this.router.navigate([LOGIN_URL]);
        }, 30000);
      },
      () => {
        this.snackBar.openFromComponent(SomethingWrongSnackBarComponent, {
          duration: 5000,
        });

        this.showLoading = false;
      }
    );
  }

  /**
   * Toggle between showing and hiding the password repetition.
   */
  toggleShowPassword(passwordFieldId: string) {
    const isVisible = this.passwordIcon !== 'visibility';

    this.passwordIcon = isVisible ? 'visibility' : 'visibility_off';
    this.changePasswordInputType(isVisible, passwordFieldId);
  }

  /**
   * Toggle between showing and hiding the password repetition.
   */
  toggleShowPasswordRepetition(passwordFieldId: string) {
    const isVisible = this.passwordRepIcon !== 'visibility';

    this.passwordRepIcon = isVisible ? 'visibility' : 'visibility_off';
    this.changePasswordInputType(isVisible, passwordFieldId);
  }

  changePasswordInputType(isVisible: boolean, passwordFieldId: string) {
    const passwordField = document.getElementById(passwordFieldId);
    passwordField.setAttribute('type', isVisible ? 'text' : 'password');
  }
}
