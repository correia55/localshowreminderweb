import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SearchListingsComponent } from './search-listings.component';

describe('SearchListingsComponent', () => {
  let component: SearchListingsComponent;
  let fixture: ComponentFixture<SearchListingsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchListingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchListingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
