import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';

import { ApiRequestsService } from '../../services/api-requests/api-requests.service';
import { MatDialog } from '@angular/material/dialog';
import { LocalShowData } from 'src/app/structs/LocalShowData';
import { ACCESS_TOKEN_ERROR, ANTICIPATION_HOURS_MARGIN } from 'src/app/constants';
import { TokenService } from 'src/app/services/token/token.service';
import { AlarmFailedSnackBarComponent } from '../snack-bar/alarm-failed-snackbar-component';
import { Reminder } from 'src/app/structs/Reminder';
import { ReminderService } from 'src/app/services/reminder/reminder.service';
import { ReminderModalData } from 'src/app/structs/ReminderModalData';
import { CreateReminderModalComponent } from '../create-reminder-modal/create-reminder-modal.component';
import { UpdateReminderModalComponent } from '../update-reminder-modal/update-reminder-modal.component';
import { Utilities } from 'src/app/utilities';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-display-sessions',
  templateUrl: './display-sessions.component.html',
  styleUrls: ['./display-sessions.component.css']
})
export class DisplaySessionsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;

  // References to the templates with the translated labels of the paginator
  @ViewChild('itemsPerPage') itemsPerPage: ElementRef;
  @ViewChild('previousPage') previousPage: ElementRef;
  @ViewChild('nextPage') nextPage: ElementRef;
  @ViewChild('of') of: ElementRef;

  public dataSource = new MatTableDataSource<LocalShowData>();
  public showData;

  // Needed by the html
  public displayedColumns: string[];
  public showLoading: boolean;
  public searchPerformed = false;

  pageSize: number;

  isSmallMobileDevice: boolean;

  @Input() public showId: number;
  @Input() public isMovie: boolean;
  @Input() public searchText: string;

  constructor(private apiRequestsService: ApiRequestsService,
    private tokenService: TokenService, private dialog: MatDialog,
    private snackBar: MatSnackBar, private reminderService: ReminderService) {
    this.isSmallMobileDevice = window.matchMedia('(max-width: 1279px)').matches;

    this.reminderService.reminders.subscribe(reminders => this.updateResultsWithReminders(reminders));
  }

  ngOnInit() {
    if (this.isSmallMobileDevice) {
      this.displayedColumns = ['wholeInfo', 'date', 'reminder'];
      this.pageSize = 5;
    } else {
      if (this.isMovie) {
        this.displayedColumns = ['title', 'channel', 'date', 'reminder'];
      } else {
        this.displayedColumns = ['title', 'season', 'episode', 'channel', 'date',
          'reminder'];
      }
      this.pageSize = 20;
    }

    this.searchListings(0);
  }

  /**
   * Make the request for a search in the listings.
   * @param callRepetition the current call of this function.
   */
  private searchListings(callRepetition: number) {
    if (callRepetition > 1) {
      console.log('There was an error, we were unable to search listings!');
      return;
    }

    if (callRepetition === 0) {
      this.showLoading = true;
    }

    try {
      var prepareResponse;

      if (this.showId) {
        prepareResponse = this.apiRequestsService.prepareSearchListingsById(this.showId, this.isMovie);
      } else {
        prepareResponse = this.apiRequestsService.prepareSearchListings(this.searchText);
      }

      if (prepareResponse[0]) {
        prepareResponse[1].subscribe(
          response => {
            this.showData = response.show;
            this.updateResults(response.show_list);
          },
          error => {
            console.log('ERROR');
            this.updateResults([]);
          }
        );
      } else {
        prepareResponse[1].subscribe(
          response => {
            this.tokenService.saveAccessToken(response.token);
            this.searchListings(callRepetition + 1);
          },
          // Refresh token is no longer valid
          error => {
            if (error.error !== ACCESS_TOKEN_ERROR) {
              this.tokenService.deleteRefreshToken();
            }

            this.showLoading = false;
            this.dataSource.data = [];
            this.searchPerformed = true;

            // Call the function again, since the token is optional in this request
            this.searchListings(callRepetition + 1);
          }
        );
      }
    } catch (error) {
      console.log(error);

      this.showLoading = false;
    }
  }

  /**
   * Do everything related with the update of the list of results.
   *
   * @param searchResults the list of results.
   */
  private updateResults(searchResults) {
    // Sort the list by date
    searchResults.sort((a, b) => {
      return a.date_time < b.date_time ? -1 : 1;
    });
      
    this.dataSource.data = searchResults;
    this.updateResultsWithReminders(this.reminderService.reminders.getValue());

    this.dataSource.paginator = this.paginator;
    this.paginator.firstPage();
    this.setPaginatorLabels();

    this.showLoading = false;
    this.searchPerformed = true;
  }

  /**
   * Set the paginator labels with the correct translations.
   * Source: https://stackoverflow.com/questions/47593692/how-to-translate-mat-paginator-in-angular-4#47594193
   * Answer from NatoBoram
   */
  setPaginatorLabels() {
    this.paginator._intl.itemsPerPageLabel = this.itemsPerPage.nativeElement.innerText;
    this.paginator._intl.previousPageLabel = this.previousPage.nativeElement.innerText;
    this.paginator._intl.nextPageLabel = this.nextPage.nativeElement.innerText;

    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number): string => {
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;

      return (startIndex + 1) + ' - ' + endIndex + ' ' + this.of.nativeElement.innerText + ' ' + length;
    };
  }

  /**
   * Used when the table page is changed.
   */
  onPageChange() {
    Utilities.scrollToTheTop(this.isSmallMobileDevice);
  }

  /**
   * Update the results of the search to the database with the match type,
   * given the list of reminders.
   *
   * @param reminders the list of reminders.
   */
  public updateResultsWithReminders(reminders: Reminder[]): void {
    for (const show of this.dataSource.data) {
      show.isMatch = false;
      show.reminder = null;

      for (const reminder of reminders) {
        if (show.id === reminder.session_id) {
          show.isMatch = true;
          show.reminder = reminder;
        }
      }
    }
  }

  /**
   * When the user creates a new reminder.
   *
   * @param event the event that triggers this.
   * @param show the corresponding show.
   */
  public onCreateReminder(event: Event, show: LocalShowData) {
    const data = ReminderModalData.createFromLocalShowData(show);

    if (!this.tokenService.validateRefreshToken()) {
      this.snackBar.openFromComponent(AlarmFailedSnackBarComponent, {
        duration: 5000
      });

      return;
    }

    if (data.reminderId == null) {
      this.dialog.open(CreateReminderModalComponent, {
        data, width: '500px'
      });
    } else {
      this.dialog.open(UpdateReminderModalComponent, {
        data, width: '500px'
      });
    }
  }

  /**
   * When the user deletes a reminder.
   *
   * @param event the event that triggers this.
   * @param show the corresponding show.
   */
  public onDeleteReminder(event: Event, show: LocalShowData) {
    const data = ReminderModalData.createFromLocalShowData(show);

    this.reminderService.deleteReminder(data.reminderId, this.snackBar, 0);
  }

  /**
   * Check if a reminder can be created for a given session.
   *
   * @param date_time the datetime for a given session.
   */
  public checkCanCreateReminder(date_time: string) {
    var now = new Date();
    now.setHours(now.getHours() + 1 + ANTICIPATION_HOURS_MARGIN);

    return now > new Date(date_time);
  }
}
