import { Component, Input } from '@angular/core';
import { DbShowData } from 'src/app/structs/DbShowData';
import { MatDialog } from '@angular/material/dialog';
import { ShowModalComponent } from '../show-modal/show-modal.component';

@Component({
  selector: 'app-db-show-card',
  templateUrl: './db-show-card.component.html',
  styleUrls: ['./db-show-card.component.css']
})
export class DbShowCardComponent {
  @Input() public show: DbShowData;
  @Input() public searchText: string;

  constructor(private dialog: MatDialog) {
  }

  /**
   * Open a dialog to show the details.
   *
   * @param show the current show.
   */
  public onShowDetails(show: DbShowData) {
    this.dialog.open(ShowModalComponent, {
      data: [show, this.searchText], width: '900px'
    });
  }
}
