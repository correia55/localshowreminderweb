import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UpdateReminderModalComponent } from './update-reminder-modal.component';

describe('UpdateReminderModalComponent', () => {
  let component: UpdateReminderModalComponent;
  let fixture: ComponentFixture<UpdateReminderModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateReminderModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateReminderModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
