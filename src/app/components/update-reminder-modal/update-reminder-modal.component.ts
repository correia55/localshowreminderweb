import { Component, OnInit, Input, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ReminderModalData } from 'src/app/structs/ReminderModalData';
import { ReminderService } from 'src/app/services/reminder/reminder.service';
import { anticipationHoursValidator, differentValueValidator } from '../shared/form-validators.directive';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-update-reminder-modal',
  templateUrl: './update-reminder-modal.component.html',
  styleUrls: ['./update-reminder-modal.component.css']
})
export class UpdateReminderModalComponent implements OnInit {
  // The form
  updateReminderForm: FormGroup;

  @Input() public data: ReminderModalData;

  constructor(@Inject(MAT_DIALOG_DATA) public modalData: ReminderModalData,
              private dialogRef: MatDialogRef<UpdateReminderModalComponent>,
              private reminderService: ReminderService,
              private snackBar: MatSnackBar) {
              this.data = modalData;
  }

  ngOnInit() {
    this.createFormGroup();
  }

  private createFormGroup() {
    this.updateReminderForm = new FormGroup({
      'anticipationHours': new FormControl(this.data.reminderAnticipationHours, {
        validators: [
          Validators.required,
          Validators.min(1),
          Validators.max(24),
          differentValueValidator(this.data.reminderAnticipationHours),
          anticipationHoursValidator(this.data.dateTime)
        ],
        updateOn: 'change'
      })
    });
  }

  get anticipationHours(): AbstractControl { return this.updateReminderForm.get('anticipationHours'); }

  get anticipationHoursValue(): number {
    // No need for trim, given that this is a number
    return this.anticipationHours.value;
  }

  onUpdateReminder() {
    if (!this.data.dateTime) {
      console.log('Invalid data!');
      return;
    }

    this.data.reminderAnticipationHours = this.anticipationHoursValue;

    this.reminderService.updateReminder(this.data, this.snackBar, 0);

    this.dialogRef.close();
  }

  onDeleteReminder() {
    this.reminderService.deleteReminder(this.data.reminderId, this.snackBar, 0);

    this.dialogRef.close();
  }
}
