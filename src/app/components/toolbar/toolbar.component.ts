import { Component, EventEmitter, Output } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SEARCH_SHOW_DB_URL, MAIN_URL, ABOUT_URL, PRIVACY_POLICY_URL, HIGHLIGHTS_URL } from 'src/app/constants';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent {
  // The form
  searchForm: FormGroup;

  @Output() public sidenavToggle = new EventEmitter();

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.createFormGroup();
  }

  private createFormGroup() {
    this.searchForm = new FormGroup({
      'searchText': new FormControl('', {
        validators: [
          Validators.required
        ],
        updateOn: 'change'
      })
    });
  }

  get searchText(): AbstractControl { return this.searchForm.get('searchText'); }

  get searchTextValue() { return this.searchText.value.trim(); }

  set setSearchText(searchText: string) { this.searchText.setValue(searchText); }

  public onSearch() {
    this.router.navigate([SEARCH_SHOW_DB_URL, this.searchTextValue]);
  }

  public onGoToMain() {
    this.router.navigate([MAIN_URL]);
  }

  public onGoToHighlights() {
    this.router.navigate([HIGHLIGHTS_URL]);
  }

  public onGoToAbout() {
    this.router.navigate([ABOUT_URL]);
  }

  public onGoToPrivacyPolicy() {
    this.router.navigate([PRIVACY_POLICY_URL]);
  }

  onToggleSidenav() {
    this.sidenavToggle.emit();
  }
}
