import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { TokenService } from '../../services/token/token.service';
import { ApiRequestsService } from '../../services/api-requests/api-requests.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AlarmService } from 'src/app/services/alarm/alarm.service';
import { SIGNUP_URL, EMAIL_CONFIRMATION_URL, UNAUTHORIZED_ACCESS, PASSWORD_RECOVERY_URL } from 'src/app/constants';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { passwordPolicyValidator } from '../shared/form-validators.directive';
import { SettingsService } from 'src/app/services/settings/settings.service';
import { SendVerificationEmailService } from 'src/app/services/send-verification-email/send-verification-email.service';
import { BooleanWrapper } from 'src/app/booleanWrapper';

import { SocialAuthService } from "angularx-social-login";
import { GoogleLoginProvider } from "angularx-social-login";
import { Observable } from 'rxjs';
import { CredentialsSnackBarComponent } from '../snack-bar/credentials-snackbar-component';
import { UnverifiedAccountSnackBarComponent } from '../snack-bar/unverified-account-snackbar-component';
import { SomethingWrongSnackBarComponent } from '../snack-bar/something-wrong-snackbar-component';
import { ChannelsService } from 'src/app/services/channels/channels.service';


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  // The form
  public loginForm: FormGroup;

  // The icons in front of each password
  public passwordIcon = 'visibility_off';

  public loginUnverifiedAccount = false;

  // Whether to show a loading or not
  public showLoading: BooleanWrapper;

  private previousURL: string;

  constructor(private route: ActivatedRoute, private router: Router,
    private apiRequestsService: ApiRequestsService,
    public tokenService: TokenService, private snackBar: MatSnackBar,
    public settingsService: SettingsService,
    private alarmService: AlarmService,
    private emailService: SendVerificationEmailService,
    private channelsService: ChannelsService,
    private socialAuthService: SocialAuthService) {
    this.showLoading = new BooleanWrapper(false);
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.previousURL = params.get('url');

      if (!this.previousURL || this.previousURL.includes(EMAIL_CONFIRMATION_URL)) {
        this.previousURL = '/';
      }

      // This redirects the user when it already has logged in
      if (this.tokenService.validateRefreshToken()) {
        this.router.navigate([this.previousURL]);
      }
    });

    this.createLoginFormGroup();
  }

  private createLoginFormGroup() {
    this.loginForm = new FormGroup({
      'email': new FormControl('', {
        validators: [
          Validators.required,
          Validators.pattern('[^@]+@[^@]+.[^@.]+')
        ],
        updateOn: 'change'
      }),
      'password': new FormControl('', {
        validators: [
          Validators.required,
          passwordPolicyValidator()
        ],
        updateOn: 'change'
      })
    });
  }

  get email(): AbstractControl { return this.loginForm.get('email'); }

  get password(): AbstractControl { return this.loginForm.get('password'); }

  get emailValue(): string { return this.email.value.trim(); }

  get passwordValue(): string { return this.password.value.trim(); }

  onSubmit() {
    this.onLogin();
  }

  /**
   * When the user performs the log in.
   */
  onLogin() {
    this.treatLoginObservable(this.apiRequestsService.prepareUserLogin(this.emailValue, this.passwordValue));
  }

  /**
   * When the user clicks a link.
   *
   * @param key the key of the url to navigate to.
   */
  public onNavigate(key: string) {
    switch (key) {
      case 'signup':
        this.router.navigate([SIGNUP_URL]);
        break;

      case 'forgotPassword':
        this.router.navigate([PASSWORD_RECOVERY_URL, this.emailValue]);
        break;
    }
  }

  /**
   * When the user clicks to send a new verification email.
   */
  public onSendNewVerificationEmail() {
    this.emailService.sendNewVerificationEmail(this.emailValue, this.snackBar, this.showLoading);
  }

  /**
   * Toggle between showing and hiding the password repetition.
   */
  toggleShowPassword(passwordFieldId: string) {
    const isVisible = this.passwordIcon !== 'visibility';

    this.passwordIcon = isVisible ? 'visibility' : 'visibility_off';
    this.changePasswordInputType(isVisible, passwordFieldId);
  }

  changePasswordInputType(isVisible: boolean, passwordFieldId: string) {
    const passwordField = document.getElementById(passwordFieldId);
    passwordField.setAttribute('type', isVisible ? 'text' : 'password');
  }

  /**
   * Sign in using Google.
   */
  public signInWithGoogle(): void {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID)
      .then(response => {
        this.onExternalSignIn(response.idToken, 'GOOGLE');
      })
      .catch(err => console.log(err));
  }

  /**
   * When the user performs the sign in using an external source.
   */
  onExternalSignIn(token: string, source: string) {
    this.treatLoginObservable(this.apiRequestsService.prepareUserExternalLogin(token, source));
  }

  /**
   * Take care of any sign in, whether it is with email or from an external source.
   * @param signInObservable the sign in observable.
   */
  treatLoginObservable(signInObservable: Observable<any>) {
    // Show loading wheel
    this.showLoading.bool = true;

    signInObservable.subscribe(
      // When the request is a success
      response => {
        // Save the information from the response
        this.tokenService.saveRefreshToken(response.token, response.username);

        this.alarmService.getAlarmList();
        this.settingsService.getSettingsList();
        this.channelsService.getChannelList();

        // Navigate to the url where the user was before logging in
        this.router.navigate([this.previousURL]);

        // Hide loading wheel
        this.showLoading.bool = false;
      },
      // When there's an error - open a snack bar with a message
      error => {
        if (error.error === UNAUTHORIZED_ACCESS) {
          this.snackBar.openFromComponent(CredentialsSnackBarComponent, {
            duration: 5000,
          });
        } else if (error.error === 'Unverified Account') {
          this.loginUnverifiedAccount = true;

          this.snackBar.openFromComponent(UnverifiedAccountSnackBarComponent, {
            duration: 5000,
          });
        } else {
          this.snackBar.openFromComponent(SomethingWrongSnackBarComponent, {
            duration: 5000,
          });
        }

        // Hide loading wheel
        this.showLoading.bool = false;
      }
    );
  }
}
