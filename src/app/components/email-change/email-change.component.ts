import { Component, OnInit } from '@angular/core';
import { ApiRequestsService } from 'src/app/services/api-requests/api-requests.service';
import { TokenService } from 'src/app/services/token/token.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ACCESS_TOKEN_ERROR, SIGNUP_URL } from 'src/app/constants';
import { UsersService } from 'src/app/services/users/users.service';
import { CheckEmailSnackBarComponent } from '../snack-bar/check-email-snackbar-component';
import { SomethingWrongSnackBarComponent } from '../snack-bar/something-wrong-snackbar-component';

@Component({
  selector: 'app-email-change',
  templateUrl: './email-change.component.html',
  styleUrls: ['./email-change.component.css']
})
export class EmailChangeComponent implements OnInit {
  public showLoading: boolean;

  // The form
  emailChangeForm: FormGroup;

  private token: string;

  constructor(private route: ActivatedRoute, private router: Router,
    private apiRequestsService: ApiRequestsService, private usersService: UsersService,
    public tokenService: TokenService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.token = params.get('token');
    });

    this.createEmailChangeForm();
  }

  private createEmailChangeForm() {
    this.emailChangeForm = new FormGroup({
      'email': new FormControl('', {
        validators: [
          Validators.required,
          Validators.pattern('[^@]+@[^@]+.[^@.]+')
        ],
        updateOn: 'change'
      })
    });
  }

  get email(): AbstractControl { return this.emailChangeForm.get('email'); }

  get emailValue(): string { return this.email.value.trim(); }

  /**
   * When the user clicks the change button.
   */
  public clickingOnChange(callRepetition: number) {
    if (this.token.indexOf('@') > -1) {
      this.fixEmailBeforeVerification();
      return;
    }

    if (callRepetition > 1) {
      console.log('There was an error, while changing the email!');

      this.showLoading = false;
      return;
    }

    if (callRepetition === 0) {
      this.showLoading = true;
    }

    if (!this.token) {
      console.log('There was an error, while changing the email!');

      this.showLoading = false;
      return;
    }

    // Prepare the change email request
    const prepareResponse = this.apiRequestsService.prepareNewEmailChange(this.token, this.emailValue);

    this.showLoading = true;

    if (prepareResponse[0]) {
      prepareResponse[1].subscribe(
        response => {
          this.snackBar.openFromComponent(CheckEmailSnackBarComponent, {
            duration: 5000,
          });

          this.showLoading = false;
        },
        error => {
          this.snackBar.openFromComponent(SomethingWrongSnackBarComponent, {
            duration: 5000,
          });

          this.showLoading = false;
        });
    } else {
      prepareResponse[1].subscribe(
        response => {
          this.tokenService.saveAccessToken(response.token);
          this.clickingOnChange(callRepetition + 1);
        },
        // Refresh token is no longer valid
        error => {
          if (error.error !== ACCESS_TOKEN_ERROR) {
            this.tokenService.deleteRefreshToken();
          }

          this.snackBar.openFromComponent(SomethingWrongSnackBarComponent, {
            duration: 5000,
          });

          this.showLoading = false;
        }
      );
    }
  }

  /**
   * Fix the user's email, but only before verification has been done.
   */
  fixEmailBeforeVerification() {
    const currentEmail = this.token;

    // Prepare the send new verification email request
    const prepareResponse = this.usersService.prepareFixEmail(currentEmail, this.emailValue);

    this.showLoading = true;

    prepareResponse.subscribe(
      response => {
        this.snackBar.openFromComponent(CheckEmailSnackBarComponent, {
          duration: 5000,
        });

        this.showLoading = false;

        this.router.navigate([SIGNUP_URL, this.emailValue]);
      },
      error => {
        this.snackBar.openFromComponent(SomethingWrongSnackBarComponent, {
          duration: 5000,
        });

        this.showLoading = false;
      }
    );
  }
}
