import { Component, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import {
  MAIN_URL, SEARCH_SHOW_DB_URL, REMINDER_URL,
  LOGIN_URL, SIGNUP_URL, ABOUT_URL, SETTINGS_URL, ALARM_URL, PRIVACY_POLICY_URL, HIGHLIGHTS_URL
} from 'src/app/constants';
import { TokenService } from 'src/app/services/token/token.service';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.css']
})
export class SidenavListComponent {
  @Output() sidenavClose = new EventEmitter();

  constructor(private router: Router, public tokenService: TokenService) { }

  public onGoToMain() {
    this.onSidenavClose();

    this.router.navigate([MAIN_URL]);
  }

  public onGoToAbout() {
    this.onSidenavClose();

    this.router.navigate([ABOUT_URL]);
  }

  public onGoToPrivacyPolicy() {
    this.onSidenavClose();

    this.router.navigate([PRIVACY_POLICY_URL]);
  }

  public onSearch() {
    this.onSidenavClose();

    this.router.navigate([SEARCH_SHOW_DB_URL]);
  }

  public onGoToHighlights() {
    this.onSidenavClose();

    this.router.navigate([HIGHLIGHTS_URL]);
  }

  public onAlarms() {
    this.onSidenavClose();

    this.router.navigate([ALARM_URL]);
  }

  public onReminders() {
    this.onSidenavClose();

    this.router.navigate([REMINDER_URL]);
  }

  public onLogIn() {
    this.onSidenavClose();

    this.router.navigate([LOGIN_URL]);
  }

  public onSignUp() {
    this.onSidenavClose();

    this.router.navigate([SIGNUP_URL]);
  }

  public onGoToSettings() {
    this.onSidenavClose();

    this.router.navigate([SETTINGS_URL]);
  }

  onSidenavClose() {
    this.sidenavClose.emit();
  }
}
