import { AbstractControl, ValidatorFn, FormGroup } from '@angular/forms';
import { ANTICIPATION_HOURS_MARGIN } from 'src/app/constants';

export function passwordPolicyValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    return (control.value === null || control.value.length < 4) ? { 'passwordPolicy': true } : null;
  };
}

export function differentValueValidator(value: any): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    return control.value === value ? { 'differentValue': true } : null;
  };
}

export function anticipationHoursValidator(date_time: Date): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    var now = new Date();
    now.setHours(now.getHours() + control.value + ANTICIPATION_HOURS_MARGIN);

    return now > new Date(date_time) ? { 'tooLate': true } : null;
  };
}

export function controlsEqualsValidator(oneKey: string, otherKey: string): ValidatorFn {
  return (group: FormGroup): { [key: string]: any } | null => {
    return group.controls[oneKey].value !== group.controls[otherKey].value ? { 'equals': true } : null;
  };
}

export function controlsDifferentValidator(oneKey: string, otherKey: string): ValidatorFn {
  return (group: FormGroup): { [key: string]: any } | null => {
    return group.controls[oneKey].value === group.controls[otherKey].value ? { 'different': true } : null;
  };
}

export function anyValueChanged(keyList: string[], valueList: string[]): ValidatorFn {
  return (group: FormGroup): { [key: string]: any } | null => {
    if (keyList.length != valueList.length) {
      return { 'invalid': true };
    }

    for(var i = 0; i < keyList.length; i++){
      if (group.controls[keyList[i]].value != valueList[i]){
        return null;
      }
    }

    return { 'equals': true };
  };
}
