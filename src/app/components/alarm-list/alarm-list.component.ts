import { Component, Input, TemplateRef, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MAIN_URL } from 'src/app/constants';
import { TokenService } from 'src/app/services/token/token.service';
import { Router } from '@angular/router';
import { AlarmService } from 'src/app/services/alarm/alarm.service';
import { Alarm } from 'src/app/structs/Alarm';
import { AlarmModalData } from 'src/app/structs/AlarmModalData';
import { UpdateAlarmModalComponent } from '../update-alarm-modal/update-alarm-modal.component';
import { Utilities } from 'src/app/utilities';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-alarm-list',
  templateUrl: './alarm-list.component.html',
  styleUrls: ['./alarm-list.component.css']
})
export class AlarmListComponent implements AfterViewInit {
  @Input() public searchDatabaseInfo: TemplateRef<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  // References to the templates with the translated labels of the paginator
  @ViewChild('itemsPerPage') itemsPerPage: ElementRef;
  @ViewChild('previousPage') previousPage: ElementRef;
  @ViewChild('nextPage') nextPage: ElementRef;
  @ViewChild('of') of: ElementRef;

  // Needed to be accessible by the html
  public alarmsDataSource = new MatTableDataSource<Alarm>();
  public alarmDisplayedColumns: string[];

  isSmallMobileDevice: boolean;

  pageSize: number;

  constructor(private dialog: MatDialog, public tokenService: TokenService,
    private snackBar: MatSnackBar, public alarmService: AlarmService,
    private router: Router) {
    this.isSmallMobileDevice = window.matchMedia('(max-width: 1279px)').matches;
    const isLoggedIn = this.tokenService.validateRefreshToken();

    // If not logged in, return to main page
    if (!isLoggedIn) {
      this.router.navigate([MAIN_URL]);
    }

    this.alarmService.alarms.subscribe(response => this.alarmsDataSource.data = response);

    if (this.isSmallMobileDevice) {
      this.alarmDisplayedColumns = ['showId', 'showInfo', 'alarm'];

      this.pageSize = 8;
    } else {
      this.alarmDisplayedColumns = ['showId', 'isMovie', 'showSeason',
        'showEpisode', 'alarm'];

      this.pageSize = 20;
    }
  }

  /** The paginator is only obtained after view init, thus why this is here. */
  ngAfterViewInit() {
    this.alarmsDataSource.paginator = this.paginator;
    this.setPaginatorLabels();
  }

  /**
   * When the user updates a alarm.
   *
   * @param show the corresponding show.
   */
  public onUpdateAlarm(alarm: Alarm) {
    const data = AlarmModalData.createFromAlarm(alarm);

    if (!data.showIsMovie) {
      this.dialog.open(UpdateAlarmModalComponent, {
        data, width: '500px'
      });
    } else {
      this.alarmService.updateAlarm(data, this.snackBar, 0);
    }
  }

  /**
   * When the user deletes a alarm.
   *
   * @param show the corresponding show.
   */
  public onDeleteAlarm(alarm: Alarm) {
    const data = AlarmModalData.createFromAlarm(alarm);

    this.alarmService.deleteAlarm(data.alarmId, this.snackBar, 0);
  }

  /**
   * Set the paginator labels with the correct translations.
   * Source: https://stackoverflow.com/questions/47593692/how-to-translate-mat-paginator-in-angular-4#47594193
   * Answer from NatoBoram
   */
  setPaginatorLabels() {
    this.paginator._intl.itemsPerPageLabel = this.itemsPerPage.nativeElement.innerText;
    this.paginator._intl.previousPageLabel = this.previousPage.nativeElement.innerText;
    this.paginator._intl.nextPageLabel = this.nextPage.nativeElement.innerText;

    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number): string => {
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;

      return (startIndex + 1) + ' - ' + endIndex + ' ' + this.of.nativeElement.innerText + ' ' + length;
    };
  }

  /**
   * Used when the table page is changed.
   */
  onPageChange() {
    Utilities.scrollToTheTop(this.isSmallMobileDevice);
  }
}
