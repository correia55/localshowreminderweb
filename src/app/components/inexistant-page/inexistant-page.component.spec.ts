import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InexistantPageComponent } from './inexistant-page.component';

describe('InexistantPageComponent', () => {
  let component: InexistantPageComponent;
  let fixture: ComponentFixture<InexistantPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InexistantPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InexistantPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
