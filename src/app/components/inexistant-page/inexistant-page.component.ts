import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inexistant-page',
  templateUrl: './inexistant-page.component.html',
  styleUrls: ['./inexistant-page.component.css']
})
export class InexistantPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
