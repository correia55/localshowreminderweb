import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  MAIN_URL, LOGIN_URL, SIGNUP_URL, SEARCH_LISTINGS_URL, SEARCH_SHOW_DB_URL,
  REMINDER_URL, ABOUT_URL, EMAIL_CONFIRMATION_URL, SETTINGS_URL,
  EMAIL_CHANGE_URL, PASSWORD_CHANGE_URL, PASSWORD_RECOVERY_URL,
  PASSWORD_RECOVERY_CHANGE_URL, ALARM_URL, PRIVACY_POLICY_URL, HIGHLIGHTS_URL
} from './constants';

import { AboutComponent } from './components/about/about.component';
import { EmailChangeComponent } from './components/email-change/email-change.component';
import { EmailConfirmationComponent } from './components/email-confirmation/email-confirmation.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { MainComponent } from './components/main/main.component';
import { ReminderListComponent } from './components/reminder-list/reminder-list.component';
import { SearchShowDBComponent } from './components/search-show-db/search-show-db.component';
import { SettingsComponent } from './components/settings/settings.component';
import { PasswordChangeComponent } from './components/password-change/password-change.component';
import { PasswordRecoveryComponent } from './components/password-recovery/password-recovery.component';
import { PasswordRecoveryChangeComponent } from './components/password-recovery-change/password-recovery-change.component';
import { InexistantPageComponent } from './components/inexistant-page/inexistant-page.component';
import { RegistrationFormComponent } from './components/registration-form/registration-form.component';
import { AlarmListComponent } from './components/alarm-list/alarm-list.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { HighlightsComponent } from './components/highlights/highlights.component';

const routes: Routes = [
  { path: ABOUT_URL, component: AboutComponent },
  { path: EMAIL_CHANGE_URL + '/:token', component: EmailChangeComponent },
  { path: EMAIL_CONFIRMATION_URL + '/:mode/:token', component: EmailConfirmationComponent },
  { path: HIGHLIGHTS_URL, component: HighlightsComponent },
  { path: LOGIN_URL, component: LoginFormComponent },
  { path: LOGIN_URL + '/:url', component: LoginFormComponent },
  { path: MAIN_URL, component: MainComponent },
  { path: PASSWORD_CHANGE_URL, component: PasswordChangeComponent },
  { path: PASSWORD_RECOVERY_URL, component: PasswordRecoveryComponent },
  { path: PASSWORD_RECOVERY_URL + '/:email', component: PasswordRecoveryComponent },
  { path: PASSWORD_RECOVERY_CHANGE_URL + '/:token', component: PasswordRecoveryChangeComponent },
  { path: ALARM_URL, component: AlarmListComponent },
  { path: REMINDER_URL, component: ReminderListComponent },
  // { path: SEARCH_LISTINGS_URL, component: SearchListingsComponent },
  // { path: SEARCH_LISTINGS_URL + '/:searchText', component: SearchListingsComponent },
  { path: SEARCH_SHOW_DB_URL, component: MainComponent },
  { path: SEARCH_SHOW_DB_URL + '/:searchText', component: SearchShowDBComponent },
  { path: SETTINGS_URL, component: SettingsComponent },
  { path: SIGNUP_URL, component: RegistrationFormComponent },
  { path: SIGNUP_URL + '/:email', component: RegistrationFormComponent },
  { path: PRIVACY_POLICY_URL, component: PrivacyPolicyComponent },
  { path: '**', component: InexistantPageComponent }
  // If we want no parameters, the url can not have a slash (/) at the end
  // { path: 'searchDB', component: SearchDBComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
