export class BooleanWrapper {
  bool: boolean;

  constructor(bool: boolean) {
    this.bool = bool;
  }
}
