// The urls to the pages of the website
export const MAIN_URL = '';
export const EMAIL_CONFIRMATION_URL = 'email-confirmation';
export const LOGIN_URL = 'login';
export const PASSWORD_RECOVERY_URL = 'recover-password';
export const PASSWORD_RECOVERY_CHANGE_URL = 'recover-password-change';
export const SIGNUP_URL = 'signup';
export const SEARCH_LISTINGS_URL = 'search-tv-listings';
export const SEARCH_SHOW_DB_URL = 'search-show-db';
export const ALARM_URL = 'alarm';
export const REMINDER_URL = 'reminder';
export const SETTINGS_URL = 'settings';
export const SHOW_URL = 'show';
export const ABOUT_URL = 'about';
export const EMAIL_CHANGE_URL = 'email-change';
export const PASSWORD_CHANGE_URL = 'password-change';
export const PRIVACY_POLICY_URL = 'privacy-policy';
export const HIGHLIGHTS_URL = 'highlights';

// A list of pages restricted to logged in user
export const RESTRICTED_PAGES = [ALARM_URL, REMINDER_URL, SETTINGS_URL,
  EMAIL_CHANGE_URL, EMAIL_CONFIRMATION_URL];

export const UNAUTHORIZED_ACCESS = 'Unauthorized Access';

// The url to the server
export const SERVER_URL = 'https://localshowreminder.pythonanywhere.com';
// export const SERVER_URL = 'http://localhost:5000';

// Timeout for each request to the server
export const REQUEST_TIMEOUT = 30000;

// Time before showing the loading screen - prevent showing loading screen for fast loadings
export const REQUEST_LOADING_THRESHOLD = 100;

// The default duration for a snackbar
export const SNACKBAR_DURATION = 5000;

// The error obtained when the token expires
export const ACCESS_TOKEN_ERROR = 'Invalid Token';

// The minimum number of hours of margin for a reminder
export const ANTICIPATION_HOURS_MARGIN = 2;
